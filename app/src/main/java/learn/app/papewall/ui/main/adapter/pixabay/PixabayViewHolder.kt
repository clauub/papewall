package learn.app.papewall.ui.main.adapter.pixabay

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import learn.app.papewall.R
import learn.app.papewall.databinding.PixabayListPhotoItemBinding
import learn.app.papewall.utils.callback.PixabayPhotoClickCallback

class PixabayViewHolder(internal val binding: PixabayListPhotoItemBinding) : RecyclerView.ViewHolder(binding.root) {

    companion object {
        fun create(parent: ViewGroup, pixabayPhotoClickCallback: PixabayPhotoClickCallback): PixabayViewHolder {
            val binding: PixabayListPhotoItemBinding = DataBindingUtil
                .inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.pixabay_list_photo_item,
                    parent, false
                )
            with(binding) {
                poster = photoCard
                callback = pixabayPhotoClickCallback
//                profile = authorImage
            }
            return PixabayViewHolder(binding)
        }
    }
}