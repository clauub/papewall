package learn.app.papewall.ui.detail.unsplash

import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import learn.app.papewall.di.scopes.ActivityScoped
import learn.app.papewall.di.scopes.FragmentScoped
import learn.app.papewall.model.Photo
import learn.app.papewall.model.PixabayPhoto
import learn.app.papewall.ui.detail.pixabay.PixabayDetailActivity
import learn.app.papewall.ui.detail.pixabay.PixabayDetailFragment
import learn.app.papewall.ui.detail.unsplash.DetailActivity.Companion.EXTRA_PHOTO

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS", "RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
@Module
abstract class DetailModule {


    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun detailFragment(): DetailFragment

    @Module
    companion object {
        @Provides
        @ActivityScoped
        @JvmStatic
        internal fun provideUnsplashPhoto(activity: DetailActivity): Photo =
            activity.intent.extras.getParcelable(EXTRA_PHOTO)
    }
}
