package learn.app.papewall.ui.main.viewmodel

import android.app.Application
import android.content.Context
import android.os.Bundle
import androidx.databinding.ObservableBoolean
import androidx.fragment.app.Fragment
import androidx.lifecycle.*
import androidx.lifecycle.Transformations.map
import androidx.lifecycle.Transformations.switchMap
import com.google.firebase.analytics.FirebaseAnalytics
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import learn.app.papewall.R
import learn.app.papewall.model.Photo
import learn.app.papewall.model.PixabayPhoto
import learn.app.papewall.repository.bypage.PageKeyRepository
import learn.app.papewall.repository.bypage.PhotosRemoteDataSource
import learn.app.papewall.repository.bypagesearch.SearchPageKeyRepository
import learn.app.papewall.repository.bypagesearchpixabay.SearchPageKeyRepoPixabay
import learn.app.papewall.ui.main.randomphoto.RandomPhoto
import learn.app.papewall.utils.*
import learn.app.papewall.utils.Constants.PHOTO_TYPE
import learn.app.papewall.utils.Constants.PORTRAIT
import learn.app.papewall.utils.Constants.VERTICAL
import learn.app.papewall.utils.Constants.WALLPAPERS_STRING
import timber.log.Timber
import java.util.*
import java.util.concurrent.Executors


class PhotosViewModel @JvmOverloads constructor(
    private val dataSource: PhotosRemoteDataSource,
    sortType: SortType? = null,
    private val context: Context
) : ViewModel() {


    val photo = MutableLiveData<PixabayPhoto>()

    private var firebaseAnalytics = FirebaseAnalytics.getInstance(context)

    fun logEventSearchEvent(query: String) {
        val params = Bundle()
        params.putString("searched_wallpaper", query)
        firebaseAnalytics.logEvent("searched_wallpaper", params)
    }

    //=======================================================
    //    GET RESULTS FOR NEW&FEATURE FRAGMENT
    //=======================================================
    // thread pool used for network requests
    @Suppress("PrivatePropertyName")
    private val NETWORK_IO = Executors.newFixedThreadPool(5)

    private val query = MutableLiveData<String>()
    private val repoResult = map(query) {
        PageKeyRepository(
            dataSource = dataSource,
            sortType = sortType,
            networkExecutor = NETWORK_IO
        ).getPhotos(it, 20)
    }

    val photos = switchMap(repoResult) { it.pagedList }!!
    val networkState = switchMap(repoResult) { it.networkState }!!
    val refreshState = switchMap(repoResult) { it.refreshState }!!

    fun refresh() {
        repoResult.value?.refresh?.invoke()
    }

    //=======================================================
    //    GET RESULTS OF SEARCH FRAGMENT BY KEYWORD
    //=======================================================
    private val repoResultSearch = map(query) {
        SearchPageKeyRepository(
            dataSource = dataSource,
            networkExecutor = NETWORK_IO
        ).getPexelsPhotos(it, 20)
    }

    val photosSearcResults = switchMap(repoResultSearch) { it.pagedList }!!
    val networkStateSearch = switchMap(repoResultSearch) { it.networkState }!!
    val refreshStateSearch = switchMap(repoResultSearch) { it.refreshState }!!

    fun refreshSearch() {
        repoResultSearch.value?.refresh?.invoke()
    }

    fun showQuery(query: String?): Boolean {
        if (this.query.value == query) {
            return false
        }
        this.query.value = query
        return true
    }

    //===============================================================
    //    GET RESULTS OF SEARCH FRAGMENT BY KEYWORD WITH PIXABAY API
    //===============================================================
    private val repoResultSearchPixabay = map(query) {
        SearchPageKeyRepoPixabay(
            dataSource = dataSource,
            networkExecutor = NETWORK_IO
        ).getPixabayPhotos(it, 20)
    }

    val photosSearchResultsPixabay = switchMap(repoResultSearchPixabay) { it.pagedList }!!
    val networkStateSearchPixabay = switchMap(repoResultSearchPixabay) { it.networkState }!!
    val refreshStateSearchPixabay = switchMap(repoResultSearchPixabay) { it.refreshState }!!

    fun refreshSearchPixabay() {
        repoResultSearchPixabay.value?.refresh?.invoke()
    }

    fun showQueryPixabay(query: String?): Boolean {
        if (this.query.value == query) {
            return false
        }
        this.query.value = query
        return true
    }


    //=======================================================
    //    GET RANDOM PHOTOS FOR RANDOM FRAGMENT
    //=======================================================
    internal val compositeDisposable = CompositeDisposable()
    val getRandomPhotos = MutableLiveData<List<Photo>>()
    val getRandomPhotosPixabay = MutableLiveData<List<PixabayPhoto>>()
    var isLoading = MutableLiveData<Boolean>()
    var isError = MutableLiveData<Boolean>()
    private val isPhotoVisible = ObservableBoolean(false)

    fun randomPhotos() {
        val subs = dataSource.randomPhotos(30, WALLPAPERS_STRING, PORTRAIT)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                isLoading.value = true
                isError.value = false
                Timber.d("Loading")
            }
            .doOnTerminate {
                isLoading.value = false
                isError.value = false
                Timber.d("Finish")
            }
            .subscribe(
                { photo ->
                    if (photo.isNotEmpty()) {
                        isPhotoVisible.set(true)
                    }
                    this.getRandomPhotos.postValue(photo)
                }
            )
            { throwable ->
                Timber.e(throwable)
                isError.value = true
            }
        compositeDisposable.add(subs)
    }


}