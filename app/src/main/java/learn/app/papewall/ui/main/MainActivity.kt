package learn.app.papewall.ui.main

import android.app.ActivityOptions
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import com.google.android.material.navigation.NavigationView
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import learn.app.papewall.R
import learn.app.papewall.ui.main.randomphoto.RandomPhoto
import learn.app.papewall.ui.search.SearchActivity
import javax.inject.Inject
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import learn.app.papewall.ui.about.AboutActivity
import learn.app.papewall.ui.main.categories.*
import learn.app.papewall.ui.settings.SettingsActivity
import learn.app.papewall.utils.Utils
import learn.app.papewall.ui.removeads.RemoveAdsFragment
import learn.app.papewall.ui.removeads.RemoveAdsHelper
import learn.app.papewall.ui.removeads.RemoveAdsViewModel
import learn.app.papewall.utils.AdsUtils
import learn.app.papewall.utils.addFragmentToActivity
import learn.app.papewall.utils.replaceFragmentInActivity

class MainActivity : DaggerAppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    @Inject
    lateinit var chooseOption: ChooseOption

    override fun onCreate(savedInstanceState: Bundle?) {
        Utils().changeTheme(this)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        adView.loadAd(AdsUtils().adRequestBanner())
        //        Remove ads if purchased was made
        RemoveAdsHelper(adView, this).removeAds(this)

        if (savedInstanceState == null) {
            addFragmentToActivity(chooseOption, R.id.frameLayoutMain)
        }


        val toggle = ActionBarDrawerToggle(
            this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
        nav_view.setNavigationItemSelectedListener(this)

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.search_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_search -> {
                val searchMenuView: View = toolbar.findViewById(R.id.action_search)
                val options = ActivityOptions.makeSceneTransitionAnimation(
                    this,
                    searchMenuView, getString(R.string.transition_search_back)
                ).toBundle()

                val intent = Intent(
                    this,
                    SearchActivity::class.java
                ).apply {
                    action = Intent.ACTION_SEARCH
                }
                startActivity(intent, options)
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            R.id.nav_remove_ads -> {
                replaceFragmentInActivity(RemoveAdsFragment(), R.id.frameLayoutMain)
            }
            R.id.nav_about -> {
                val intent = Intent(
                    this,
                    AboutActivity::class.java
                ).apply {
                    action = Intent.ACTION_SEARCH
                }
                startActivity(intent)
            }
            R.id.nav_settings -> {
                val intent = Intent(
                    this,
                    SettingsActivity::class.java
                ).apply {
                    action = Intent.ACTION_SEARCH
                }
                startActivity(intent)
            }

//            Categories
            R.id.nav_latest -> {
                replaceFragmentInActivity(LatestPhotos(), R.id.frameLayoutMain)
            }
            R.id.nav_random -> {
                replaceFragmentInActivity(RandomPhoto(), R.id.frameLayoutMain)
            }
            R.id.nav_android -> {
                replaceFragmentInActivity(RandomPhoto(), R.id.frameLayoutMain)
            }
            R.id.nav_iphone -> {
                replaceFragmentInActivity(IPhonePhotos(), R.id.frameLayoutMain)
            }
            R.id.nav_nature -> {
                replaceFragmentInActivity(NaturePhotos(), R.id.frameLayoutMain)
            }
            R.id.nav_travel -> {
                replaceFragmentInActivity(TravelPhotos(), R.id.frameLayoutMain)
            }
            R.id.nav_food -> {
                replaceFragmentInActivity(FoodPhotos(), R.id.frameLayoutMain)
            }
            R.id.nav_arhitecture -> {
                replaceFragmentInActivity(ArhitecturePhotos(), R.id.frameLayoutMain)
            }
            R.id.nav_animals -> {
                replaceFragmentInActivity(AnimalsPhotos(), R.id.frameLayoutMain)
            }
            R.id.nav_abstract -> {
                replaceFragmentInActivity(AbstractPhotos(), R.id.frameLayoutMain)
            }
            R.id.nav_minimal -> {
                replaceFragmentInActivity(MinimalPhotos(), R.id.frameLayoutMain)
            }
            else -> throw RuntimeException("Unknown sortType")
        }
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onResume() {
        super.onResume()
        Utils().checkAndRequest(this)
    }
}
