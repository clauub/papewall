package learn.app.papewall.ui.about


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.android.support.DaggerFragment
import learn.app.papewall.R
import learn.app.papewall.databinding.FragmentAboutBinding
import learn.app.papewall.databinding.FragmentSettingsBinding
import learn.app.papewall.di.scopes.ActivityScoped
import learn.app.papewall.ui.settings.SettingsViewModel
import javax.inject.Inject

@ActivityScoped
open class AboutFragment @Inject constructor(): DaggerFragment(){

    private lateinit var viewModel: AboutViewModel

    // data binding instance
    private lateinit var dataBinding: FragmentAboutBinding

    protected open fun initViewModel() {
        viewModel = ViewModelProviders.of(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                @Suppress("UNCHECKED_CAST")
                return AboutViewModel(context = context!!) as T
            }
        })[AboutViewModel::class.java]
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
       dataBinding = FragmentAboutBinding.inflate(inflater, container, false)

        initViewModel()

        dataBinding.vm = viewModel

        return dataBinding.root
    }


}
