package learn.app.papewall.ui.base

import dagger.Module
import dagger.android.ContributesAndroidInjector
import learn.app.papewall.di.scopes.FragmentScoped
import learn.app.papewall.ui.main.ChooseOption
import learn.app.papewall.ui.main.categories.*
import learn.app.papewall.ui.main.randomphoto.RandomPhoto
import learn.app.papewall.ui.removeads.RemoveAdsFragment

@Module
abstract class MainActivityModule {

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun randomPhotosFragment(): RandomPhoto

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun natureFragment(): BaseCategoryFragment

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun androidPhotosFragment(): AndroidPhotos

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun iphonePhotosFragment(): IPhonePhotos

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun arhitecturePhotosFragment(): ArhitecturePhotos

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun carsPhotosFragment(): TravelPhotos

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun animalsPhotosFragment(): AnimalsPhotos

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun foodPhotosFragment(): FoodPhotos

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun naturePhotosFragment(): NaturePhotos

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun abstractPhotosFragment(): AbstractPhotos

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun minimalPhotosFragment(): MinimalPhotos

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun latestPhotosFragment(): LatestPhotos

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun chooseOptionFragment(): ChooseOption

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun removeAdsFragment(): RemoveAdsFragment



}
