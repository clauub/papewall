package learn.app.papewall.ui.removeads

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.view.View
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getColor
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.ads.AdView
import kotlinx.android.synthetic.main.toolbar_layout.*
import learn.app.papewall.R
import learn.app.papewall.utils.AdsUtils
import learn.app.papewall.utils.Constants
import learn.app.papewall.utils.Messages
import timber.log.Timber

class RemoveAdsHelper(private val adView: AdView, fragment: FragmentActivity) {

    private var removeAdsViewModel: RemoveAdsViewModel =
        ViewModelProviders.of(fragment).get(RemoveAdsViewModel::class.java)

    private fun hideAds(adView: AdView, entitled: Boolean) {
        if (entitled) {
            adView.visibility = View.GONE
        }
    }

    fun removeAds(lifecycleOwner: LifecycleOwner) {
        removeAdsViewModel.premiumCarLiveData.observe(lifecycleOwner, Observer {
            it?.apply { hideAds(adView, entitled) }
        })
    }
}

class RemoveAdsHelperFragment(private val adView: AdView, fragment: Fragment) {
    private var removeAdsViewModel: RemoveAdsViewModel =
        ViewModelProviders.of(fragment).get(RemoveAdsViewModel::class.java)

    private fun hideAds(adView: AdView, entitled: Boolean) {
        if (entitled) {
            adView.visibility = View.GONE
        }
    }


    fun removeAds(lifecycleOwner: LifecycleOwner) {
        removeAdsViewModel.premiumCarLiveData.observe(lifecycleOwner, Observer {
            it?.apply { hideAds(adView, entitled) }
        })
    }
}