package learn.app.papewall.ui.main.adapter.pexels

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import learn.app.papewall.R
import learn.app.papewall.databinding.PexelsListPhotoItemBinding
import learn.app.papewall.utils.callback.PexelsPhotoClickCallback
import learn.app.papewall.utils.callback.PixabayPhotoClickCallback

class PexelsViewHolder(internal val binding: PexelsListPhotoItemBinding) : RecyclerView.ViewHolder(binding.root) {

    companion object {
        fun create(parent: ViewGroup, pexelsPhotoClickCallback: PexelsPhotoClickCallback): PexelsViewHolder {
            val binding: PexelsListPhotoItemBinding = DataBindingUtil
                .inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.pexels_list_photo_item,
                    parent, false
                )
            with(binding) {
                poster = photoCard
                callback = pexelsPhotoClickCallback
//                profile = authorImage
            }
            return PexelsViewHolder(binding)
        }
    }
}