package learn.app.papewall.ui.main.categories

import learn.app.papewall.R
import learn.app.papewall.di.scopes.ActivityScoped
import learn.app.papewall.utils.Constants
import javax.inject.Inject
import java.util.*


@ActivityScoped
open class LatestPhotos @Inject constructor(): BaseCategoryFragment() {

    override fun initViewModel() {
        super.initViewModel()

        val array = context!!.resources.getStringArray(R.array.wallpapers)

        val randomStr = array[Random().nextInt(array.size)]

        viewModel.showQueryPixabay(randomStr)
    }
}