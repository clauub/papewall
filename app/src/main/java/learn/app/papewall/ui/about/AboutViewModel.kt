package learn.app.papewall.ui.about

import android.content.Context
import androidx.lifecycle.ViewModel
import android.widget.Toast
import android.content.Intent
import android.widget.TimePicker
import androidx.core.content.ContextCompat.startActivity
import learn.app.papewall.BuildConfig
import learn.app.papewall.R
import learn.app.papewall.utils.Messages
import learn.app.papewall.utils.Messages.toastLong
import timber.log.Timber
import java.lang.Exception
import android.net.Uri
import androidx.core.content.ContextCompat.startActivity
import android.content.ActivityNotFoundException
import learn.app.papewall.utils.Utils

class AboutViewModel(private val context: Context) : ViewModel() {

    var urlUnsplash = "https://unsplash.com/"
    var urlDonation = "https://www.buymeacoffee.com/jnXOE2lef"
    var versionCode = BuildConfig.VERSION_NAME

    fun sendEmail() {
        val i = Intent(Intent.ACTION_SEND)
        i.type = "message/rfc822"
        i.putExtra(Intent.EXTRA_EMAIL, arrayOf(context.getString(R.string.email)))
        i.putExtra(Intent.EXTRA_SUBJECT, "")
        i.putExtra(Intent.EXTRA_TEXT, "")
        try {
            context.startActivity(Intent.createChooser(i, context.getString(R.string.contact_us)))
        } catch (ex: android.content.ActivityNotFoundException) {
            toastLong(context, R.string.cleaned_cache)
            Timber.d(ex.toString())
        }
    }

    fun shareApp() {
        try {
            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.app_name))
            var shareMessage = "\n\nYou should try PapeWall:\n"
            shareMessage =
                shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "\n\n"
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
            context.startActivity(Intent.createChooser(shareIntent, context.getString(R.string.complete_action_using)))
        } catch (e: Exception) {
            Timber.d(e.toString())
        }

    }

    fun rateApp() {
        val uri = Uri.parse("market://details?id=" + context.packageName)
        val goToMarket = Intent(Intent.ACTION_VIEW, uri)
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(
            Intent.FLAG_ACTIVITY_NO_HISTORY or
                    Intent.FLAG_ACTIVITY_NEW_DOCUMENT or
                    Intent.FLAG_ACTIVITY_MULTIPLE_TASK
        )
        try {
            context.startActivity(goToMarket)
        } catch (e: ActivityNotFoundException) {
            context.startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + context.packageName)
                )
            )
        }

    }

    fun openUnsplash() {
       Utils().openUrl(context, urlUnsplash)
    }

    fun donate() {
        Utils().openUrl(context, urlDonation)
    }


}