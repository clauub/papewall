package learn.app.papewall.ui.detail.pexels


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.FrameLayout
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.Snackbar
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_detail.*
import kotlinx.android.synthetic.main.slide_panel_unsplash.view.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import learn.app.papewall.App
import learn.app.papewall.BR

import learn.app.papewall.R
import learn.app.papewall.databinding.FragmentPexelsDetailBinding
import learn.app.papewall.databinding.FragmentPixabayDetailBinding
import learn.app.papewall.di.scopes.ActivityScoped
import learn.app.papewall.model.PexelsPhoto
import learn.app.papewall.model.PixabayPhoto
import learn.app.papewall.ui.detail.pixabay.PixabayViewModel
import learn.app.papewall.ui.removeads.RemoveAdsHelper
import learn.app.papewall.ui.removeads.RemoveAdsHelperFragment
import learn.app.papewall.utils.AdsUtils
import learn.app.papewall.utils.Constants
import learn.app.papewall.utils.Messages
import learn.app.papewall.utils.Utils
import javax.inject.Inject

@Suppress("UNCHECKED_CAST")
@ActivityScoped
class PexelsDetailFragment @Inject constructor() : DaggerFragment(), View.OnClickListener {

    @Inject
    lateinit var pexelsPhoto: PexelsPhoto

    private var btnName = ""

    private lateinit var binding: FragmentPexelsDetailBinding

    private lateinit var viewModel: PexelsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        viewModel = ViewModelProviders.of(requireActivity(), object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return PexelsViewModel(requireActivity().application) as T
            }
        })[PexelsViewModel::class.java]

        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_pexels_detail, container, false)

        binding = FragmentPexelsDetailBinding.bind(root).apply {
            photo = this@PexelsDetailFragment.pexelsPhoto
            setVariable(BR.vm, viewModel)
            lifecycleOwner = this@PexelsDetailFragment
        }

        with(root) {

            //        Handle ads
            adView.loadAd(AdsUtils().adRequestBanner())
            RemoveAdsHelperFragment(adView, this@PexelsDetailFragment).removeAds(viewLifecycleOwner)

            if (!Utils().isConnected(context)) {
                Messages.toastLong(context, R.string.no_internet_connection)
            }

            iconShareImg.setOnClickListener(this@PexelsDetailFragment)
            iconSetImg.setOnClickListener(this@PexelsDetailFragment)
            iconDownloadImg.setOnClickListener(this@PexelsDetailFragment)
        }
        return root
    }

    private fun onWallpaperDownloaded(wallpaperUri: String) {
        val message = if (wallpaperUri.isNotBlank())
            R.string.saved_in_gallery
        else
            R.string.something_went_wrong

        Snackbar.make(slidePanel, message, Snackbar.LENGTH_LONG).run {
            if (message == R.string.saved_in_gallery) {
                setAction(R.string.open) {
                    viewModel.showWallpaperInGallery(wallpaperUri)
                }
            }
            show()
        }
    }

    override fun onClick(p0: View?) {
        val params = Bundle()
        params.putInt("ButtonID", view!!.id)
        when (p0?.id) {
            R.id.iconShareImg -> {
                if (Utils().isConnected(context)) {
                    Messages.snackBarLong(slidePanel, R.string.preparing_wallpaper)
                    viewModel.shareWallpaper(pexelsPhoto)
                    btnName = Constants.SHARE_WALLPAPER_PEXELS
                }
            }
            R.id.iconSetImg -> {
                if (Utils().isConnected(context)) {
//                    viewModel.insert(photo)
                    Messages.snackBarLong(slidePanel, R.string.preparing_wallpaper)
                    viewModel.setWallpaper(pexelsPhoto, false)
                    btnName = Constants.SET_WALLPAPER_PEXELS
                }
            }
            R.id.iconDownloadImg -> {
                if (Utils().checkPermission(activity!!)) {
                    if (Utils().isConnected(context)) {
                        Messages.snackBarLong(slidePanel, R.string.preparing_wallpaper)
                        viewModel.downloadWallpaper(pexelsPhoto)
                        btnName = Constants.DOWNLOAD_WALLPAPER_PEXELS
                    }

                    viewModel.wallpaperDownloaded.observe(this, Observer {
                        onWallpaperDownloaded(it)
                    })
                }
            }
        }

        val firebaseAnalytics = (context?.applicationContext as App).getmFirebaseAnalytics()
        firebaseAnalytics.logEvent(btnName, params)
    }

}
