package learn.app.papewall.ui.main.randomphoto


import android.app.Application
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityOptionsCompat
import androidx.core.content.ContextCompat
import androidx.core.util.Pair
import androidx.core.view.ViewCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_main_photo.*

import learn.app.papewall.R
import learn.app.papewall.databinding.FragmentMainPhotoBinding
import learn.app.papewall.di.scopes.ActivityScoped
import learn.app.papewall.model.Photo
import learn.app.papewall.repository.bypage.PhotosRemoteDataSource
import learn.app.papewall.ui.main.viewmodel.PhotosViewModel
import learn.app.papewall.utils.callback.PhotoClickCallback
import javax.inject.Inject
import learn.app.papewall.ui.detail.unsplash.DetailActivity
import learn.app.papewall.utils.Messages
import learn.app.papewall.utils.Messages.timberE
import learn.app.papewall.utils.SortType
import learn.app.papewall.utils.Utils
import learn.app.papewall.widget.MarginDecoration
import timber.log.Timber


@ActivityScoped
open class RandomPhoto @Inject constructor() : DaggerFragment(), PhotoClickCallback {
    fun getSortType(): SortType? = null

    @Inject
    lateinit var dataSource: PhotosRemoteDataSource

    private lateinit var viewModel: PhotosViewModel

    // data binding instance
    lateinit var dataBinding: FragmentMainPhotoBinding

    protected open fun initViewModel() {
        viewModel = ViewModelProviders.of(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                @Suppress("UNCHECKED_CAST")
                return PhotosViewModel(
                    dataSource = dataSource,
                    sortType = getSortType(), context = Application()
                ) as T
            }
        })[PhotosViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = FragmentMainPhotoBinding.inflate(inflater, container, false)

        initViewModel()

        initRandomPhotoAdapter()

        return dataBinding.root
    }

    private fun initRandomPhotoAdapter() {
        dataBinding.swipeRefresh.apply {
            setColorSchemeColors(
                ContextCompat.getColor(context, R.color.colorPrimary),
                ContextCompat.getColor(context, R.color.colorAccent),
                ContextCompat.getColor(context, R.color.colorPrimaryDark)


            )
            viewModel.isLoading.observe(this@RandomPhoto, Observer {
                isRefreshing = it
            })
            setOnRefreshListener { viewModel.randomPhotos() }
        }

        dataBinding.rvListPhoto.apply {
            addItemDecoration(MarginDecoration(context))

            setHasFixedSize(true)
        }

        dataBinding.retryButton.apply {
            setOnClickListener {
                viewModel.randomPhotos()
            }
        }

        viewModel.isError.observe(this, Observer {
            dataBinding.isLoading = it == true

            if (it == true) {
                error_msg.apply {
                    dataBinding.rvListPhoto.visibility = View.GONE
                    if (!Utils().isConnectedNoToast(context)) {
                        text = getString(R.string.no_internet_connection)
                        timberE(getString(R.string.no_internet_connection))
                    } else {
                        text = getString(R.string.error_connection)
                        timberE(getString(R.string.error_connection))
                    }
                }
            }
            if (it == false) {
                rvListPhoto.visibility = View.VISIBLE
            }
        })

        viewModel.randomPhotos()

        viewModel.getRandomPhotos.observe(this@RandomPhoto, Observer<List<Photo>> {

            val adapter = RandomPhotoAdapter(it, this@RandomPhoto)

            dataBinding.rvListPhoto.adapter = adapter
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewModel.compositeDisposable.clear()
    }

    override fun onClick(photo: Photo, poster: ImageView) {
        val intent = Intent(activity, DetailActivity::class.java).apply {
            putExtras(Bundle().apply {
                putParcelable(DetailActivity.EXTRA_PHOTO, photo)
            })
        }
        val activityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(
            requireActivity(),

            // Now we provide a list of Pair items which contain the view we can transitioning
            // from, and the name of the view it is transitioning to, in the launched activity
            Pair<View, String>(poster, ViewCompat.getTransitionName(poster))
        )
//            Pair<View, String>(name, getString(R.string.view_name_header_title)))

        // Now we can start the Activity, providing the activity options as a bundle
        ActivityCompat.startActivity(requireContext(), intent, activityOptions.toBundle())
    }

}
