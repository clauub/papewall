package learn.app.papewall.ui.about

import dagger.Module
import dagger.android.ContributesAndroidInjector
import learn.app.papewall.di.scopes.FragmentScoped
import learn.app.papewall.ui.about.AboutFragment
import learn.app.papewall.ui.main.categories.AbstractPhotos

@Module
abstract class AboutModule {

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun aboutFragment(): AboutFragment

}
