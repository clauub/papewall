package learn.app.papewall.ui.detail.pexels

import android.app.Application
import android.app.WallpaperManager
import android.content.Intent
import android.net.Uri
import android.provider.MediaStore
import androidx.core.content.FileProvider
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cafe.adriel.androidcoroutinescopes.viewmodel.CoroutineScopedAndroidViewModel
import com.bumptech.glide.Glide
import com.google.firebase.analytics.FirebaseAnalytics
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import learn.app.papewall.BuildConfig
import learn.app.papewall.R
import learn.app.papewall.model.PexelsPhoto
import learn.app.papewall.model.PixabayPhoto
import java.io.File
import android.graphics.BitmapFactory
import android.graphics.Bitmap

class PexelsViewModel(private val context: Application) : CoroutineScopedAndroidViewModel(context) {

    companion object {
        private const val PROVIDER_AUTHORITY = "${BuildConfig.APPLICATION_ID}.provider"
        private const val MIME_TYPE_IMAGE = "image/jpeg"
        private const val DEFAULT_FILE_NAME = "PapeWall.jpg"
    }

    private val _wallpaperUpdated = MutableLiveData<Boolean>()
    private val _wallpaperDownloaded = MutableLiveData<String>()

    val wallpaperUpdated: LiveData<Boolean> get() = _wallpaperUpdated
    val wallpaperDownloaded: LiveData<String> get() = _wallpaperDownloaded

    fun downloadWallpaper(photo: PexelsPhoto) {
        launch {
            try {
                val wallpaperUri = saveWallpaperInGallery(photo)
                _wallpaperDownloaded.value = wallpaperUri
            } catch (e: Exception) {
                e.printStackTrace()
                _wallpaperDownloaded.value = ""
            }
        }
    }

    fun setWallpaper(photo: PexelsPhoto, quick: Boolean = false) {
        launch {
            try {
                val wallpaperFile = getWallpaperFile(photo)
                if (quick) {
                    setWallpaper(wallpaperFile)
                    _wallpaperUpdated.value = true
                } else {
                    showSetWallpaperOptions(wallpaperFile)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                _wallpaperUpdated.value = false
            }
        }
    }

    fun shareWallpaper(photo: PexelsPhoto) {
        launch {
            try {
                val context = context
                val wallpaperFile = getWallpaperFile(photo)
                val uri = FileProvider.getUriForFile(
                    context,
                    PROVIDER_AUTHORITY, wallpaperFile
                )
                val intent = Intent(Intent.ACTION_SEND).apply {
                    addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    putExtra(Intent.EXTRA_STREAM, uri)
                    type = MIME_TYPE_IMAGE
                }
                context.startActivity(
                    Intent.createChooser(
                        intent,
                        context.getString(R.string.share_with)
                    ).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                )
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun showSetWallpaperOptions(wallpaperFile: File) {
        val context = context
        val uri = FileProvider.getUriForFile(
            context,
            PROVIDER_AUTHORITY, wallpaperFile
        )
        val sharingIntent = Intent(Intent.ACTION_ATTACH_DATA).apply {
            setDataAndType(uri, MIME_TYPE_IMAGE)
            addCategory(Intent.CATEGORY_DEFAULT)
            addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            putExtra("mimeType", MIME_TYPE_IMAGE)
        }
        val chooserIntent = Intent.createChooser(sharingIntent, "Set as...")
        chooserIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        context.startActivity(chooserIntent)
    }

    private suspend fun setWallpaper(wallpaperFile: File) = withContext(Dispatchers.IO) {
        WallpaperManager.getInstance(context)
            .setStream(wallpaperFile.inputStream())
    }

    private suspend fun getWallpaperFile(photo: PexelsPhoto): File = withContext(Dispatchers.IO) {
        val file = Glide.with(context)
            .downloadOnly()
            .load(photo.src?.portrait)
            .submit()
            .get()
        val renamedFile = File(
            file.parent,
            DEFAULT_FILE_NAME
        )

        file.renameTo(renamedFile)
        renamedFile
    }

    private suspend fun saveWallpaperInGallery(photo: PexelsPhoto): String = withContext(Dispatchers.IO) {
        val context = context
        val wallpaperFile = getWallpaperFile(photo)
        val wallpaperUri = MediaStore.Images.Media.insertImage(
            context.contentResolver,
            wallpaperFile.path, wallpaperFile.name, "By ${photo.photographer}"
        )
        val intent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE).apply {
            data = Uri.parse(wallpaperUri)
        }
        context.sendBroadcast(intent)

        wallpaperUri
    }

    fun showWallpaperInGallery(wallpaperUri: String) {
        Intent(Intent.ACTION_VIEW).run {
            try {
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                setDataAndType(
                    Uri.parse(wallpaperUri),
                    MIME_TYPE_IMAGE
                )
                context.startActivity(this)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }


}