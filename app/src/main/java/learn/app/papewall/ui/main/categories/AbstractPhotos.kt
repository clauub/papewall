package learn.app.papewall.ui.main.categories

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.toolbar_layout.*
import learn.app.papewall.databinding.FragmentMainPhotoBinding
import learn.app.papewall.di.scopes.ActivityScoped
import learn.app.papewall.ui.main.MainActivity
import learn.app.papewall.utils.Constants
import javax.inject.Inject


@Suppress("UNREACHABLE_CODE")
@ActivityScoped
open class AbstractPhotos @Inject constructor(): BaseCategoryFragment() {

    override fun initViewModel() {
        super.initViewModel()
        viewModel.showQueryPixabay(Constants.ABSTRACT_WALLPAPERS)
    }
}