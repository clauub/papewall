package learn.app.papewall.ui.detail.pixabay

import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import learn.app.papewall.di.scopes.ActivityScoped
import learn.app.papewall.di.scopes.FragmentScoped
import learn.app.papewall.model.PixabayPhoto
import learn.app.papewall.ui.detail.pixabay.PixabayDetailActivity.Companion.EXTRA_PHOTO_PIXABAY

@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS", "NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
@Module
abstract class PixabayDetailModule {

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun pixabayDetailFragment(): PixabayDetailFragment


    @Module
    companion object {
        @Provides
        @ActivityScoped
        @JvmStatic
        internal fun providePixabayPhoto(activity: PixabayDetailActivity): PixabayPhoto =
            activity.intent.extras.getParcelable(EXTRA_PHOTO_PIXABAY)
    }
}
