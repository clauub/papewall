package learn.app.papewall.ui.settings


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_settings.*
import learn.app.papewall.R
import learn.app.papewall.databinding.FragmentSettingsBinding
import learn.app.papewall.di.scopes.ActivityScoped
import learn.app.papewall.utils.SharedPref
import learn.app.papewall.utils.Utils
import javax.inject.Inject


@ActivityScoped
open class SettingsFragment @Inject constructor() : DaggerFragment() {

    private var sharedpref: SharedPref? = null

    private lateinit var viewModel: SettingsViewModel

    // data binding instance
    private lateinit var dataBinding: FragmentSettingsBinding

    protected open fun initViewModel() {
        viewModel = ViewModelProviders.of(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                @Suppress("UNCHECKED_CAST")
                return SettingsViewModel(context = context!!) as T
            }
        })[SettingsViewModel::class.java]
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        dataBinding = FragmentSettingsBinding.inflate(inflater, container, false)

        initViewModel()

        dataBinding.vm = viewModel

        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

       changeTheme()

        if (sharedpref?.loadNightModeState() == true) {
            switchTheme.isChecked = true
        }

        switchTheme.setOnCheckedChangeListener { _, b ->
            if (b) {
                sharedpref?.setNightModeState(true)
                Utils().restartActivityFromFragment(activity!!)
            } else {
                sharedpref?.setNightModeState(false)
                Utils().restartActivityFromFragment(activity!!)
            }
        }
    }

    private fun changeTheme() {
        sharedpref = SharedPref(context!!)
        if (sharedpref?.loadNightModeState() == true) {
            activity!!.setTheme(R.style.anotherstyle)
        } else  activity!!.setTheme(R.style.AppTheme)
    }


}
