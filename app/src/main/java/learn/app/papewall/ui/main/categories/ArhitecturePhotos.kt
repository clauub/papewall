package learn.app.papewall.ui.main.categories

import learn.app.papewall.di.scopes.ActivityScoped
import learn.app.papewall.utils.Constants
import javax.inject.Inject

@ActivityScoped
open class ArhitecturePhotos @Inject constructor(): BaseCategoryFragment() {

    override fun initViewModel() {
        super.initViewModel()
        viewModel.showQueryPixabay(Constants.ARHITECTURE_WALLPAPERS)
    }
}