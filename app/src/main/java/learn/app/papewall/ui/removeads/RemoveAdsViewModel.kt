package learn.app.papewall.ui.removeads

import android.app.Activity
import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import learn.app.papewall.repository.billing.localdb.AugmentedSkuDetails
import learn.app.papewall.repository.billing.localdb.PremiumCar
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel
import learn.app.papewall.repository.billing.BillingRepository
import timber.log.Timber

class RemoveAdsViewModel(application: Application) : AndroidViewModel(application) {

    val inappSkuDetailsListLiveData: LiveData<List<AugmentedSkuDetails>>
    val premiumCarLiveData: LiveData<PremiumCar>

    private val viewModelScope = CoroutineScope(Job() + Dispatchers.Main)
    private val repository: BillingRepository = BillingRepository.getInstance(application)

    init {
        repository.startDataSourceConnections()
        premiumCarLiveData = repository.premiumCarLiveData
        inappSkuDetailsListLiveData = repository.inappSkuDetailsListLiveData
    }

    fun makePurchase(activity: Activity, skuDetails: AugmentedSkuDetails) {
        repository.launchBillingFlow(activity, skuDetails)
    }


    override fun onCleared() {
        super.onCleared()
        Timber.d("onCleared")
        repository.endDataSourceConnections()
        viewModelScope.coroutineContext.cancel()
    }
}