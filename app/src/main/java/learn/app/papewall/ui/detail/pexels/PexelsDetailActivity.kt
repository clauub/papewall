package learn.app.papewall.ui.detail.pexels

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import dagger.android.support.DaggerAppCompatActivity
import learn.app.papewall.R
import learn.app.papewall.ui.detail.pixabay.PixabayDetailFragment
import learn.app.papewall.ui.detail.unsplash.DetailFragment
import learn.app.papewall.utils.addFragmentToActivity
import javax.inject.Inject

class PexelsDetailActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var pexelsFragment: PexelsDetailFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pexels_detail)

        if (savedInstanceState == null) {
            supportFragmentManager.findFragmentById(R.id.fragment_container)
                    as DetailFragment? ?: pexelsFragment.also {
                addFragmentToActivity(it, R.id.fragment_container)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        const val EXTRA_PHOTO_PEXELS = "photo_pexels"
    }
}
