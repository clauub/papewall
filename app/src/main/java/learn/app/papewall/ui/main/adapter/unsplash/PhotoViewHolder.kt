package learn.app.papewall.ui.main.adapter.unsplash

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import learn.app.papewall.R
import learn.app.papewall.databinding.UnsplashListPhotoItemBinding
import learn.app.papewall.utils.callback.PhotoClickCallback

class PhotoViewHolder(internal val binding: UnsplashListPhotoItemBinding) : RecyclerView.ViewHolder(binding.root) {

    companion object {
        fun create(parent: ViewGroup, photoClickCallback: PhotoClickCallback): PhotoViewHolder {
            val binding: UnsplashListPhotoItemBinding = DataBindingUtil
                .inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.unsplash_list_photo_item,
                    parent, false
                )
            with(binding) {
                poster = photoCard
                callback = photoClickCallback
//                profile = authorImage
            }
            return PhotoViewHolder(binding)
        }
    }
}