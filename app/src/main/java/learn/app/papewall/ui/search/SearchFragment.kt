package learn.app.papewall.ui.search


import android.app.Application
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityOptionsCompat
import androidx.core.content.ContextCompat
import androidx.core.util.Pair
import androidx.core.view.ViewCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.paging.PagedList
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_main_photo.*

import learn.app.papewall.R
import learn.app.papewall.databinding.FragmentPexelsPhotoBinding
import learn.app.papewall.databinding.FragmentPixabayPhotoBinding
import learn.app.papewall.di.scopes.ActivityScoped
import learn.app.papewall.model.PexelsPhoto
import learn.app.papewall.model.PixabayPhoto
import learn.app.papewall.repository.NetworkState
import learn.app.papewall.repository.Status
import learn.app.papewall.repository.bypage.PhotosRemoteDataSource
import learn.app.papewall.ui.detail.pexels.PexelsDetailActivity
import learn.app.papewall.ui.detail.pixabay.PixabayDetailActivity
import learn.app.papewall.ui.detail.unsplash.DetailActivity
import learn.app.papewall.ui.main.adapter.pexels.PexelsPhotoAdapter
import learn.app.papewall.ui.main.viewmodel.PhotosViewModel
import learn.app.papewall.ui.main.adapter.unsplash.UnsplashPhotoAdapter
import learn.app.papewall.utils.Messages.snackBarLong
import learn.app.papewall.utils.Messages.timberE
import learn.app.papewall.utils.SortType
import learn.app.papewall.utils.Utils
import learn.app.papewall.utils.callback.PexelsPhotoClickCallback
import learn.app.papewall.utils.callback.PixabayPhotoClickCallback
import learn.app.papewall.widget.MarginDecoration
import timber.log.Timber
import javax.inject.Inject

@ActivityScoped
open class SearchFragment @Inject constructor() : DaggerFragment(), PexelsPhotoClickCallback {
    fun getSortType(): SortType? = null

    @Inject
    lateinit var dataSource: PhotosRemoteDataSource

    private lateinit var viewModel: PhotosViewModel

    // data binding instance
    lateinit var dataBinding: FragmentPexelsPhotoBinding

    protected open fun initViewModel() {
        viewModel = ViewModelProviders.of(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                @Suppress("UNCHECKED_CAST")
                return PhotosViewModel(
                    dataSource = dataSource,
                    sortType = getSortType(), context = Application()
                ) as T
            }
        })[PhotosViewModel::class.java]
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dataBinding = FragmentPexelsPhotoBinding.inflate(inflater, container, false)

        initViewModel()

        initAdapter()

        return dataBinding.root
    }

    private fun initAdapter() {
        dataBinding.swipeRefresh.apply {
            setColorSchemeColors(
                ContextCompat.getColor(context, R.color.colorPrimary),
                ContextCompat.getColor(context, R.color.colorAccent),
                ContextCompat.getColor(context, R.color.colorPrimaryDark)
            )

            viewModel.refreshStateSearch.observe(this@SearchFragment, Observer {
                isRefreshing = it == NetworkState.LOADING
            })
            setOnRefreshListener { viewModel.refreshSearch() }
        }

        dataBinding.retryButton.apply {
            setOnClickListener {
                viewModel.refreshSearch()
            }
        }

        val adapter = PexelsPhotoAdapter(this)

        dataBinding.rvListPhoto.apply {
            addItemDecoration(MarginDecoration(context))

            setHasFixedSize(true)

            dataBinding.rvListPhoto.adapter = adapter
        }

        viewModel.photosSearcResults.observe(
            this,
            Observer<PagedList<PexelsPhoto>> {
                if (it != null && it.size > 0) {
                    Timber.d("Results: ${it.size}")
                    adapter.submitList(it)
                } else {
                    Timber.d("No images found")
                    snackBarLong(swipe_refresh, R.string.no_images_found)
                }
            })

        viewModel.networkStateSearch.observe(this, Observer<NetworkState> {
            adapter.setNetworkState(it)

            dataBinding.isLoading = it?.status == Status.FAILED

            if (it?.status == Status.FAILED) {

                error_msg.apply {
                    if (!Utils().isConnectedNoToast(context)) {
                        text = getString(R.string.no_internet_connection)
                        timberE(it.msg)
                    } else {
                        text = getString(R.string.error_connection)
                        timberE(it.msg)
                    }
                }
            }
        })
    }

    fun searchViewClicked(query: String?) {
        if (viewModel.showQuery(query)) {
            rvListPhoto.scrollToPosition(0)
            (rvListPhoto.adapter as? UnsplashPhotoAdapter)?.submitList(null)

            //Firebase Analytics -- (search_wallpapers event)
            viewModel.logEventSearchEvent(query!!)
        }
    }

    override fun onClick(photo: PexelsPhoto, poster: ImageView) {
        Timber.d("result: $photo")
        val intent = Intent(activity, PexelsDetailActivity::class.java).apply {
            putExtras(Bundle().apply {
                putParcelable(PexelsDetailActivity.EXTRA_PHOTO_PEXELS, photo)
            })
        }
        val activityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(
            requireActivity(),

            // Now we provide a list of Pair items which contain the view we can transitioning
            // from, and the name of the view it is transitioning to, in the launched activity
            Pair<View, String>(poster, ViewCompat.getTransitionName(poster))
        )
//            Pair<View, String>(name, getString(R.string.view_name_header_title)))

        // Now we can start the Activity, providing the activity options as a bundle
        ActivityCompat.startActivity(requireContext(), intent, activityOptions.toBundle())
    }
}

