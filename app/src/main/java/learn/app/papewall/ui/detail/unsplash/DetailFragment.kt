package learn.app.papewall.ui.detail.unsplash


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.Snackbar
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_detail.*
import kotlinx.android.synthetic.main.slide_panel_unsplash.view.*
import learn.app.papewall.BR

import learn.app.papewall.R
import learn.app.papewall.databinding.FragmentDetailBinding
import learn.app.papewall.di.scopes.ActivityScoped
import learn.app.papewall.model.Photo
import learn.app.papewall.utils.Constants.DOWNLOAD_WALLPAPER_UNSPLASH
import learn.app.papewall.utils.Constants.SET_WALLPAPER_UNSPLASH
import learn.app.papewall.utils.Constants.SHARE_WALLPAPER_UNSPLASH
import learn.app.papewall.utils.Messages.snackBarLong
import learn.app.papewall.utils.Messages.toastLong
import learn.app.papewall.utils.Utils
import javax.inject.Inject
import learn.app.papewall.App
import android.view.Gravity
import android.R.attr.gravity
import android.app.Activity
import android.widget.FrameLayout
import androidx.fragment.app.FragmentActivity
import kotlinx.android.synthetic.main.toolbar_layout.*
import learn.app.papewall.ui.removeads.RemoveAdsHelper
import learn.app.papewall.ui.removeads.RemoveAdsHelperFragment
import learn.app.papewall.utils.AdsUtils


@Suppress("DEPRECATED_IDENTITY_EQUALS", "CAST_NEVER_SUCCEEDS")
@ActivityScoped
class DetailFragment @Inject constructor() : DaggerFragment(), View.OnClickListener {

    @Inject
    lateinit var photo: Photo

    private var btnName = ""

    private lateinit var binding: FragmentDetailBinding

    private lateinit var viewModel: PhotoDetailViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        viewModel = ViewModelProviders.of(requireActivity(), object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                @Suppress("UNCHECKED_CAST")
                return PhotoDetailViewModel(requireActivity().application) as T
            }
        })[PhotoDetailViewModel::class.java]

//        activity!!.window.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)

        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_detail, container, false)


        binding = FragmentDetailBinding.bind(root).apply {
            photo = this@DetailFragment.photo
            setVariable(BR.vm, viewModel)
            lifecycleOwner = this@DetailFragment
        }

        with(root) {

            //        Handle ads
            adView.loadAd(AdsUtils().adRequestBanner())
            RemoveAdsHelperFragment(adView, this@DetailFragment).removeAds(viewLifecycleOwner)

            if (!Utils().isConnected(context)) {
                toastLong(context, R.string.no_internet_connection)
            }

            iconShareImg.setOnClickListener(this@DetailFragment)
            iconSetImg.setOnClickListener(this@DetailFragment)
            iconDownloadImg.setOnClickListener(this@DetailFragment)
        }
        return root
    }

    private fun onWallpaperDownloaded(wallpaperUri: String) {
        val message = if (wallpaperUri.isNotBlank())
            R.string.saved_in_gallery
        else
            R.string.something_went_wrong


        Snackbar.make(slidePanel, message, Snackbar.LENGTH_LONG).run {
            if (message == R.string.saved_in_gallery) {
                setAction(R.string.open) {
                    viewModel.showWallpaperInGallery(wallpaperUri)
                }
            }
            show()
        }
    }

    override fun onClick(p0: View?) {
        val params = Bundle()
        params.putInt("ButtonID", view!!.id)
        when (p0?.id) {
            R.id.iconShareImg -> {
                if (Utils().isConnected(context)) {
                    snackBarLong(slidePanel, R.string.preparing_wallpaper)
                    viewModel.shareWallpaper(photo)
                    btnName = SHARE_WALLPAPER_UNSPLASH
                }
            }
            R.id.iconSetImg -> {
                if (Utils().isConnected(context)) {
//                    viewModel.insert(photo)
                    snackBarLong(slidePanel, R.string.preparing_wallpaper)
                    viewModel.setWallpaper(photo, false)
                    btnName = SET_WALLPAPER_UNSPLASH
                }
            }
            R.id.iconDownloadImg -> {
                if (Utils().checkPermission(activity!!)) {
                    if (Utils().isConnected(context)) {
                        snackBarLong(slidePanel, R.string.preparing_wallpaper)
                        viewModel.downloadWallpaper(photo)
                        btnName = DOWNLOAD_WALLPAPER_UNSPLASH
                    }

                    viewModel.wallpaperDownloaded.observe(this@DetailFragment, Observer {
                        onWallpaperDownloaded(it)
                    })
                }
            }
        }

        val firebaseAnalytics = (context?.applicationContext as App).getmFirebaseAnalytics()
        firebaseAnalytics.logEvent(btnName, params)
    }
}
