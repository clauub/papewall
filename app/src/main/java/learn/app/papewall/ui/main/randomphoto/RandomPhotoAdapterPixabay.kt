package learn.app.papewall.ui.main.randomphoto

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import learn.app.papewall.R
import learn.app.papewall.databinding.PixabayListPhotoItemBinding
import learn.app.papewall.databinding.UnsplashListPhotoItemBinding
import learn.app.papewall.model.Photo
import learn.app.papewall.model.PixabayPhoto
import learn.app.papewall.utils.callback.PhotoClickCallback
import learn.app.papewall.utils.callback.PixabayPhotoClickCallback

class RandomPhotoAdapterPixabay(private val randomPhoto: List<PixabayPhoto>,
                         private val pixabayPhotoClickCallback: PixabayPhotoClickCallback
):
    RecyclerView.Adapter<RandomPhotoAdapterPixabay.RandomPhotoHolder>() {

    override fun onBindViewHolder(holder: RandomPhotoHolder, position: Int) {
        with(holder.binding) {
            photo = this@RandomPhotoAdapterPixabay.randomPhoto[position]
            poster = photoCard
            executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RandomPhotoHolder {
        val binding: PixabayListPhotoItemBinding = DataBindingUtil
            .inflate(
                LayoutInflater.from(parent.context),
                R.layout.pixabay_list_photo_item,
                parent, false)
        binding.callback = pixabayPhotoClickCallback
        return RandomPhotoHolder(binding)
    }

    override fun getItemCount() = randomPhoto.size


    class RandomPhotoHolder(internal val binding: PixabayListPhotoItemBinding) :
        RecyclerView.ViewHolder(binding.root)
}