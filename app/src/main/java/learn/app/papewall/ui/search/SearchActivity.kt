package learn.app.papewall.ui.search

import android.app.SearchManager
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.text.InputType
import android.view.inputmethod.EditorInfo
import android.widget.SearchView
import com.google.firebase.analytics.FirebaseAnalytics
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_search.*
import kotlinx.android.synthetic.main.activity_search.adView
import kotlinx.android.synthetic.main.toolbar_layout.*
import learn.app.papewall.R
import learn.app.papewall.ui.removeads.RemoveAdsHelper
import learn.app.papewall.utils.AdsUtils
import learn.app.papewall.utils.SharedPref
import learn.app.papewall.utils.Utils
import learn.app.papewall.utils.addFragmentToActivity
import javax.inject.Inject

class SearchActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var searchFragment: SearchFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        Utils().changeTheme(this)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        adView.loadAd(AdsUtils().adRequestBanner())
        //        Remove ads if purchased was made
        RemoveAdsHelper(adView, this).removeAds(this)

        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        search_view.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        // hint, inputType & ime options seem to be ignored from XML! Set in code
        search_view.queryHint = getString(R.string.search_hint)
        search_view.inputType = InputType.TYPE_TEXT_FLAG_CAP_WORDS
        search_view.imeOptions = search_view.imeOptions or EditorInfo.IME_ACTION_SEARCH or
                EditorInfo.IME_FLAG_NO_EXTRACT_UI or EditorInfo.IME_FLAG_NO_FULLSCREEN

        search_back.setOnClickListener {
            search_back.background = null
            finishAfterTransition()
        }

        val fragment = supportFragmentManager.findFragmentById(R.id.fragment_container)
                as SearchFragment? ?: searchFragment.also {
            addFragmentToActivity(it, R.id.fragment_container)
        }

        search_view.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                fragment.searchViewClicked(query)
                return true
            }

            override fun onQueryTextChange(query: String): Boolean {
                return true
            }
        })

    }
}
