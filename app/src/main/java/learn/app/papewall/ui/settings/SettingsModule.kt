package learn.app.papewall.ui.settings

import dagger.Module
import dagger.android.ContributesAndroidInjector
import learn.app.papewall.di.scopes.FragmentScoped
import learn.app.papewall.ui.about.AboutFragment

@Module
abstract class SettingsModule {

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun settingsFragment(): SettingsFragment

}
