package learn.app.papewall.ui.settings

import android.os.Bundle
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_search.*
import kotlinx.android.synthetic.main.activity_search.adView
import kotlinx.android.synthetic.main.activity_settings.*
import kotlinx.android.synthetic.main.activity_settings.search_back
import kotlinx.android.synthetic.main.toolbar_layout.*
import learn.app.papewall.R
import learn.app.papewall.ui.removeads.RemoveAdsHelper
import learn.app.papewall.ui.search.SearchFragment
import learn.app.papewall.utils.AdsUtils
import learn.app.papewall.utils.Utils
import learn.app.papewall.utils.addFragmentToActivity
import javax.inject.Inject


class SettingsActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var settingsFragment: SettingsFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        Utils().changeTheme(this)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        adView_settings.loadAd(AdsUtils().adRequestBanner())
        //        Remove ads if purchased was made
        RemoveAdsHelper(adView_settings, this).removeAds(this)

        supportFragmentManager.findFragmentById(R.id.fragment_container)
                as SearchFragment? ?: settingsFragment.also {
            addFragmentToActivity(it, R.id.fragment_container)
        }

        search_back.setOnClickListener {
            search_back.background = null
            finishAfterTransition()
        }
    }


}


