package learn.app.papewall.ui.settings

import android.content.Context
import android.view.View
import androidx.lifecycle.ViewModel
import learn.app.papewall.R
import learn.app.papewall.utils.Messages.toastLong
import learn.app.papewall.utils.SharedPref
import learn.app.papewall.utils.Utils
import android.R.attr.versionName
import java.io.File
import java.nio.file.Files.isDirectory
import androidx.databinding.ObservableInt
import androidx.databinding.ObservableField
import learn.app.papewall.BuildConfig
import androidx.databinding.ObservableLong

class SettingsViewModel(private val context: Context) : ViewModel() {


    var versionCode = BuildConfig.VERSION_NAME

    fun deleteCache() {
        Utils().deleteCache(context)
        toastLong(context, R.string.cleaned_cache)
    }
}