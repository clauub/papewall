package learn.app.papewall.ui.main.randomphoto

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import learn.app.papewall.R
import learn.app.papewall.databinding.UnsplashListPhotoItemBinding
import learn.app.papewall.model.Photo
import learn.app.papewall.utils.callback.PhotoClickCallback

class RandomPhotoAdapter(private val randomPhoto: List<Photo>,
                         private val photoClickCallback: PhotoClickCallback):
        RecyclerView.Adapter<RandomPhotoAdapter.RandomPhotoHolder>() {

    override fun onBindViewHolder(holder: RandomPhotoHolder, position: Int) {
        with(holder.binding) {
            photo = this@RandomPhotoAdapter.randomPhoto[position]
            poster = photoCard
            executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RandomPhotoHolder {
        val binding: UnsplashListPhotoItemBinding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.context),
                        R.layout.unsplash_list_photo_item,
                        parent, false)
        binding.callback = photoClickCallback
        return RandomPhotoHolder(binding)
    }

    override fun getItemCount() = randomPhoto.size


    class RandomPhotoHolder(internal val binding: UnsplashListPhotoItemBinding) :
            RecyclerView.ViewHolder(binding.root)
}