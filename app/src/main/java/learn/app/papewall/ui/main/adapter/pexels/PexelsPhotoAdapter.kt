package learn.app.papewall.ui.main.adapter.pexels

import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import learn.app.papewall.model.PexelsPhoto
import learn.app.papewall.model.PixabayPhoto
import learn.app.papewall.repository.NetworkState
import learn.app.papewall.ui.main.adapter.pixabay.PixabayViewHolder
import learn.app.papewall.utils.callback.PexelsPhotoClickCallback
import learn.app.papewall.utils.callback.PixabayPhotoClickCallback
import java.util.*

class PexelsPhotoAdapter(private val pexelsPhotoClickCallback: PexelsPhotoClickCallback)
    : PagedListAdapter<PexelsPhoto, RecyclerView.ViewHolder>(POST_COMPARATOR) {

    private var networkState: NetworkState? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return PexelsViewHolder.create(parent, pexelsPhotoClickCallback)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        getItem(position)?.let {
            with((holder as PexelsViewHolder).binding) {
                photo = it
                executePendingBindings()
            }
        }
    }

    private fun hasExtraRow(): Boolean {
        return networkState != null && networkState != NetworkState.LOADED
    }

    /**
     * Set the current network state to the adapter
     * but this work only after the initial load
     * and the adapter already have list to add new loading raw to it
     * so the initial loading state the activity responsible for handle it
     *
     * @param newNetworkState the new network state
     */
    fun setNetworkState(newNetworkState: NetworkState?) {
        if (currentList != null) {
            if (currentList!!.size != 0) {
                val previousState = this.networkState
                val hadExtraRow = hasExtraRow()
                this.networkState = newNetworkState
                val hasExtraRow = hasExtraRow()
                if (hadExtraRow != hasExtraRow) {
                    if (hadExtraRow) {
                        notifyItemRemoved(super.getItemCount())
                    } else {
                        notifyItemInserted(super.getItemCount())
                    }
                } else if (hasExtraRow && previousState !== newNetworkState) {
                    notifyItemChanged(itemCount - 1)
                }
            }
        }
    }

    companion object {
        val POST_COMPARATOR = object : DiffUtil.ItemCallback<PexelsPhoto>() {
            override fun areItemsTheSame(oldItem: PexelsPhoto, newItem: PexelsPhoto): Boolean {
                return oldItem.id!! == newItem.id
            }

            override fun areContentsTheSame(oldItem: PexelsPhoto, newItem: PexelsPhoto): Boolean {
                return Objects.equals(oldItem, newItem)
            }
        }
    }
}

