package learn.app.papewall.ui.removeads


import android.app.Activity
import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.android.support.DaggerFragment
import learn.app.papewall.databinding.FragmentRemoveAdsBinding
import learn.app.papewall.di.scopes.ActivityScoped
import javax.inject.Inject
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import learn.app.papewall.repository.billing.localdb.AugmentedSkuDetails
import kotlinx.android.synthetic.main.fragment_remove_ads.view.*
import learn.app.papewall.utils.callback.BillingClickCallback
import timber.log.Timber


@ActivityScoped
open class RemoveAdsFragment @Inject constructor() : DaggerFragment(),
    BillingClickCallback {

    private lateinit var viewModel: RemoveAdsViewModel

    // data binding instance
    private lateinit var dataBinding: FragmentRemoveAdsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        dataBinding = FragmentRemoveAdsBinding.inflate(inflater, container, false)


        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val inappAdapter = object : SkuDetailsAdapter(this) {
            override fun onSkuDetailsClicked(item: AugmentedSkuDetails) {
                onPurchase(view, item)
            }
        }
        attachAdapterToRecyclerView(view.inapp_inventory, inappAdapter)

        viewModel = ViewModelProviders.of(this).get(RemoveAdsViewModel::class.java)
        viewModel.inappSkuDetailsListLiveData.observe(this, Observer { it ->
            it?.let { inappAdapter.setSkuDetailsList(it) }
        })
    }

    private fun onPurchase(view: View, skuDetails: AugmentedSkuDetails) {
        viewModel.makePurchase(activity as Activity, skuDetails)
        Timber.d("starting purchase flow for SkuDetail:\n $skuDetails")
    }

    private fun attachAdapterToRecyclerView(recyclerView: RecyclerView, skuAdapter: SkuDetailsAdapter) {
        with(recyclerView) {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = skuAdapter
        }
    }

    override fun onClick(skuDetails: AugmentedSkuDetails) {
        viewModel.makePurchase(activity as Activity, skuDetails)
    }

}
