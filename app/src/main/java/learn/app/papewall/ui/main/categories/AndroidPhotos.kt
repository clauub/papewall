package learn.app.papewall.ui.main.categories

import kotlinx.android.synthetic.main.toolbar_layout.*
import learn.app.papewall.di.scopes.ActivityScoped
import learn.app.papewall.utils.Constants
import javax.inject.Inject

@Suppress("UNREACHABLE_CODE")
@ActivityScoped
open class AndroidPhotos @Inject constructor(): BaseCategoryFragment() {

    override fun initViewModel() {
        super.initViewModel()
        viewModel.showQueryPixabay(Constants.ANDROID_WALLPAPERS)
    }

    override fun setToolbarTitle() {
        super.setToolbarTitle()
        toolbar.title = Constants.ANDROID_WALLPAPERS
    }
}