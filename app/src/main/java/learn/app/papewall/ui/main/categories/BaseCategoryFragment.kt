package learn.app.papewall.ui.main.categories

import android.app.Application
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityOptionsCompat
import androidx.core.content.ContextCompat
import androidx.core.util.Pair
import androidx.core.view.ViewCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.paging.PagedList
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_main_photo.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import learn.app.papewall.R
import learn.app.papewall.databinding.FragmentPixabayPhotoBinding
import learn.app.papewall.model.PexelsPhoto
import learn.app.papewall.model.PixabayPhoto
import learn.app.papewall.repository.NetworkState
import learn.app.papewall.repository.Status
import learn.app.papewall.repository.bypage.PhotosRemoteDataSource
import learn.app.papewall.ui.detail.pixabay.PixabayDetailActivity
import learn.app.papewall.ui.main.MainActivity
import learn.app.papewall.ui.main.adapter.pexels.PexelsPhotoAdapter
import learn.app.papewall.ui.main.adapter.pixabay.PixabayPhotoAdapter
import learn.app.papewall.ui.main.viewmodel.PhotosViewModel
import learn.app.papewall.utils.Messages
import learn.app.papewall.utils.SortType
import learn.app.papewall.utils.Utils
import learn.app.papewall.utils.callback.PexelsPhotoClickCallback
import learn.app.papewall.utils.callback.PixabayPhotoClickCallback
import learn.app.papewall.widget.MarginDecoration
import timber.log.Timber
import javax.inject.Inject

abstract class BaseCategoryFragment : DaggerFragment(), PixabayPhotoClickCallback {
    fun getSortType(): SortType? = null

    @Inject
    lateinit var dataSource: PhotosRemoteDataSource

    protected lateinit var viewModel: PhotosViewModel

    // data binding instance
    lateinit var dataBinding: FragmentPixabayPhotoBinding

    protected open fun initViewModel() {
        viewModel = ViewModelProviders.of(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                @Suppress("UNCHECKED_CAST")
                return PhotosViewModel(
                    dataSource = dataSource,
                    sortType = getSortType(), context = Application()
                ) as T
            }
        })[PhotosViewModel::class.java]
    }

    protected open fun setToolbarTitle() {

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dataBinding = FragmentPixabayPhotoBinding.inflate(inflater, container, false)

        initViewModel()

        initAdapter()

        return dataBinding.root
    }

    protected open fun initAdapter() {
        dataBinding.swipeRefresh.apply {
            setColorSchemeColors(
                ContextCompat.getColor(context, R.color.colorPrimary),
                ContextCompat.getColor(context, R.color.colorAccent),
                ContextCompat.getColor(context, R.color.colorPrimaryDark)
            )

            viewModel.refreshStateSearchPixabay.observe(this@BaseCategoryFragment, Observer {
                isRefreshing = it == NetworkState.LOADING
            })
            setOnRefreshListener { viewModel.refreshSearchPixabay() }
        }

        dataBinding.retryButton.apply {
            setOnClickListener {
                viewModel.refreshSearchPixabay()
            }
        }


        val adapter = PixabayPhotoAdapter(this)


        dataBinding.rvListPhoto.apply {
            addItemDecoration(MarginDecoration(context))

            setHasFixedSize(true)

            dataBinding.rvListPhoto.adapter = adapter
        }

        viewModel.photosSearchResultsPixabay.observe(
            this,
            Observer<PagedList<PixabayPhoto>> {
                adapter.submitList(it)
            })

        viewModel.networkStateSearchPixabay.observe(this, Observer<NetworkState> {
            adapter.setNetworkState(it)

            dataBinding.isLoading = it?.status == Status.FAILED

            if (it?.status == Status.FAILED) {

                error_msg.apply {
                    if (!Utils().isConnectedNoToast(context)) {
                        text = getString(R.string.no_internet_connection)
                        Messages.timberE(it.msg)
                    } else {
                        text = getString(R.string.error_connection)
                        Messages.timberE(it.msg)
                    }
                }
            }
        })
    }

    override fun onClick(photo: PixabayPhoto, poster: ImageView) {
        val intent = Intent(activity, PixabayDetailActivity::class.java).apply {
            putExtras(Bundle().apply {
                putParcelable(PixabayDetailActivity.EXTRA_PHOTO_PIXABAY, photo)
            })
        }
        val activityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(
            requireActivity(),

            // Now we provide a list of Pair items which contain the view we can transitioning
            // from, and the name of the view it is transitioning to, in the launched activity
            Pair<View, String>(poster, ViewCompat.getTransitionName(poster))
        )
//            Pair<View, String>(name, getString(R.string.view_name_header_title)))

        // Now we can start the Activity, providing the activity options as a bundle
        ActivityCompat.startActivity(requireContext(), intent, activityOptions.toBundle())
    }

}
