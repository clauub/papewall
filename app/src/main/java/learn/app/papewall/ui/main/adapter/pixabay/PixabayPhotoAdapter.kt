package learn.app.papewall.ui.main.adapter.pixabay

import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import learn.app.papewall.model.PixabayPhoto
import learn.app.papewall.repository.NetworkState
import learn.app.papewall.utils.callback.PixabayPhotoClickCallback
import java.util.*

class PixabayPhotoAdapter(private val pixabayPhotoClickCallback: PixabayPhotoClickCallback)
    : PagedListAdapter<PixabayPhoto, RecyclerView.ViewHolder>(POST_COMPARATOR) {

    private var networkState: NetworkState? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return PixabayViewHolder.create(parent, pixabayPhotoClickCallback)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        getItem(position)?.let {
            with((holder as PixabayViewHolder).binding) {
                photo = it
                executePendingBindings()
            }
        }
    }

    private fun hasExtraRow(): Boolean {
        return networkState != null && networkState != NetworkState.LOADED
    }

    /**
     * Set the current network state to the adapter
     * but this work only after the initial load
     * and the adapter already have list to add new loading raw to it
     * so the initial loading state the activity responsible for handle it
     *
     * @param newNetworkState the new network state
     */
    fun setNetworkState(newNetworkState: NetworkState?) {
        if (currentList != null) {
            if (currentList!!.size != 0) {
                val previousState = this.networkState
                val hadExtraRow = hasExtraRow()
                this.networkState = newNetworkState
                val hasExtraRow = hasExtraRow()
                if (hadExtraRow != hasExtraRow) {
                    if (hadExtraRow) {
                        notifyItemRemoved(super.getItemCount())
                    } else {
                        notifyItemInserted(super.getItemCount())
                    }
                } else if (hasExtraRow && previousState !== newNetworkState) {
                    notifyItemChanged(itemCount - 1)
                }
            }
        }
    }

    companion object {
        val POST_COMPARATOR = object : DiffUtil.ItemCallback<PixabayPhoto>() {
            override fun areItemsTheSame(oldItem: PixabayPhoto, newItem: PixabayPhoto): Boolean {
                return oldItem.id!! == newItem.id
            }

            override fun areContentsTheSame(oldItem: PixabayPhoto, newItem: PixabayPhoto): Boolean {
                return Objects.equals(oldItem, newItem)
            }
        }
    }
}

