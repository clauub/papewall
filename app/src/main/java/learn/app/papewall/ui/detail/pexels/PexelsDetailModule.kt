package learn.app.papewall.ui.detail.pexels

import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import learn.app.papewall.di.scopes.ActivityScoped
import learn.app.papewall.di.scopes.FragmentScoped
import learn.app.papewall.model.PexelsPhoto
import learn.app.papewall.model.PixabayPhoto
import learn.app.papewall.ui.detail.pixabay.PixabayDetailActivity
import learn.app.papewall.ui.detail.pixabay.PixabayDetailFragment


@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS", "RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
@Module
abstract class PexelsDetailModule {

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun pexelsDetailFragment(): PexelsDetailFragment


    @Module
    companion object {
        @Provides
        @ActivityScoped
        @JvmStatic
        internal fun providePexelsPhoto(activity: PexelsDetailActivity): PexelsPhoto =
            activity.intent.extras.getParcelable(PexelsDetailActivity.EXTRA_PHOTO_PEXELS)
    }
}