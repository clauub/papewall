package learn.app.papewall.ui.about

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_about.*
import kotlinx.android.synthetic.main.activity_search.*
import kotlinx.android.synthetic.main.activity_search.search_back
import kotlinx.android.synthetic.main.toolbar_layout.*
import learn.app.papewall.R
import learn.app.papewall.ui.removeads.RemoveAdsHelper
import learn.app.papewall.ui.search.SearchFragment
import learn.app.papewall.utils.AdsUtils
import learn.app.papewall.utils.SharedPref
import learn.app.papewall.utils.Utils
import learn.app.papewall.utils.addFragmentToActivity
import javax.inject.Inject


class AboutActivity : DaggerAppCompatActivity() {
    @Inject
    lateinit var aboutFragment: AboutFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        Utils().changeTheme(this)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)

        adView_about.loadAd(AdsUtils().adRequestBanner())
        //        Remove ads if purchased was made
        RemoveAdsHelper(adView_about, this).removeAds(this)

        search_back.setOnClickListener {
            search_back.background = null
            finishAfterTransition()
        }

        supportFragmentManager.findFragmentById(R.id.fragment_container)
                as SearchFragment? ?: aboutFragment.also {
            addFragmentToActivity(it, R.id.fragment_container)
        }
    }
}

