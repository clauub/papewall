package learn.app.papewall.ui.main.categories

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import learn.app.papewall.di.scopes.ActivityScoped
import learn.app.papewall.utils.Constants
import javax.inject.Inject

@ActivityScoped
open class FoodPhotos @Inject constructor(): BaseCategoryFragment() {

    override fun initViewModel() {
        super.initViewModel()
        viewModel.showQueryPixabay(Constants.FOOD_WALLPAPERS)
    }
}