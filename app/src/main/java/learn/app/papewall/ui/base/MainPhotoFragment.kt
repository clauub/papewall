package learn.app.papewall.ui.base

import android.app.Application
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityOptionsCompat
import androidx.core.content.ContextCompat
import androidx.core.util.Pair
import androidx.core.view.ViewCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.paging.PagedList
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_main_photo.*
import learn.app.papewall.R
import learn.app.papewall.api.services.UnsplashService
import learn.app.papewall.databinding.FragmentMainPhotoBinding
import learn.app.papewall.model.Photo
import learn.app.papewall.repository.NetworkState
import learn.app.papewall.repository.bypage.PhotosRemoteDataSource
import learn.app.papewall.repository.Status
import learn.app.papewall.ui.detail.unsplash.DetailActivity
import learn.app.papewall.ui.detail.unsplash.DetailActivity.Companion.EXTRA_PHOTO
import learn.app.papewall.utils.callback.PhotoClickCallback
import learn.app.papewall.ui.main.viewmodel.PhotosViewModel
import learn.app.papewall.ui.main.adapter.unsplash.UnsplashPhotoAdapter
import learn.app.papewall.utils.SortType
import learn.app.papewall.widget.MarginDecoration
import timber.log.Timber
import javax.inject.Inject


abstract class MainPhotoFragment : DaggerFragment(), PhotoClickCallback {

    @Inject
    lateinit var dataSource: PhotosRemoteDataSource

    private lateinit var photoAdapter: UnsplashPhotoAdapter

    protected lateinit var viewModel: PhotosViewModel

    // data binding instance
    open lateinit var dataBinding: FragmentMainPhotoBinding

    protected abstract fun getSortType(): SortType?

    @Inject
    lateinit var photoService: UnsplashService

    protected open fun initViewModel() {
        viewModel = ViewModelProviders.of(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                @Suppress("UNCHECKED_CAST")
                return PhotosViewModel(
                    dataSource = dataSource,
                    sortType = getSortType(), context = Application()
                ) as T
            }
        })[PhotosViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = FragmentMainPhotoBinding.inflate(inflater, container, false)

        initViewModel()

        initAdapter()

        return dataBinding.root
    }

    private fun initAdapter() {
        dataBinding.swipeRefresh.apply {
            setColorSchemeColors(
                ContextCompat.getColor(context, R.color.colorPrimary),
                ContextCompat.getColor(context, R.color.colorAccent),
                ContextCompat.getColor(context, R.color.colorPrimaryDark)
            )

            viewModel.refreshState.observe(this@MainPhotoFragment, Observer {
                isRefreshing = it == NetworkState.LOADING
            })
            setOnRefreshListener { viewModel.refresh() }
        }

        dataBinding.retryButton.apply {
            setOnClickListener {
                viewModel.refresh()
            }
        }

        val adapter = UnsplashPhotoAdapter(this@MainPhotoFragment)


        dataBinding.rvListPhoto.apply {
            addItemDecoration(MarginDecoration(context))

            setHasFixedSize(true)

            dataBinding.rvListPhoto.adapter = adapter
        }

        viewModel.photos.observe(
            this@MainPhotoFragment,
            Observer<PagedList<Photo>> { adapter.submitList(it) })

        viewModel.networkState.observe(this@MainPhotoFragment, Observer<NetworkState> {
            adapter.setNetworkState(it)

            dataBinding.isLoading = it?.status == Status.FAILED

            if (it?.status == Status.FAILED) {

                error_msg.apply {
                    text = getString(R.string.error_connection)
                    Timber.e(it.msg)
                }
            }
        })
    }

    override fun onClick(photo: Photo, poster: ImageView) {
        val intent = Intent(activity, DetailActivity::class.java).apply {
            putExtras(Bundle().apply {
                putParcelable(EXTRA_PHOTO, photo)
            })
        }
        val activityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(
            requireActivity(),

            // Now we provide a list of Pair items which contain the view we can transitioning
            // from, and the name of the view it is transitioning to, in the launched activity
            Pair<View, String>(poster, ViewCompat.getTransitionName(poster))
        )
//            Pair<View, String>(name, getString(R.string.view_name_header_title)))

        // Now we can start the Activity, providing the activity options as a bundle
        ActivityCompat.startActivity(requireContext(), intent, activityOptions.toBundle())
    }
}
