package learn.app.papewall.ui.search

import dagger.Module
import dagger.android.ContributesAndroidInjector
import learn.app.papewall.di.scopes.FragmentScoped
import learn.app.papewall.ui.search.SearchFragment


@Module
abstract class SearchModule {

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun searchFragment(): SearchFragment

}
