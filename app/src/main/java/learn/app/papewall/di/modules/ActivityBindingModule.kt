package learn.app.papewall.di.modules

import dagger.Module
import dagger.android.ContributesAndroidInjector
import learn.app.papewall.di.scopes.ActivityScoped
import learn.app.papewall.ui.about.AboutActivity
import learn.app.papewall.ui.base.MainActivityModule
import learn.app.papewall.ui.about.AboutModule
import learn.app.papewall.ui.detail.pexels.PexelsDetailActivity
import learn.app.papewall.ui.detail.pexels.PexelsDetailModule
import learn.app.papewall.ui.detail.unsplash.DetailActivity
import learn.app.papewall.ui.detail.unsplash.DetailModule
import learn.app.papewall.ui.detail.pixabay.PixabayDetailActivity
import learn.app.papewall.ui.detail.pixabay.PixabayDetailModule
import learn.app.papewall.ui.search.SearchModule
import learn.app.papewall.ui.main.MainActivity
import learn.app.papewall.ui.search.SearchActivity
import learn.app.papewall.ui.settings.SettingsActivity
import learn.app.papewall.ui.settings.SettingsModule
import learn.app.papewall.ui.splash.SplashActivity

/**
 *
 *
 * this class will include all activities in our app
 */

@Module
abstract class ActivityBindingModule {

    @ActivityScoped
    @ContributesAndroidInjector(modules = [(MainActivityModule::class)])
    internal abstract fun mainActivity(): MainActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = [(DetailModule::class)])
    internal abstract fun detailActivity(): DetailActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = [(PixabayDetailModule::class)])
    internal abstract fun pixbayDetailActivity(): PixabayDetailActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = [(PexelsDetailModule::class)])
    internal abstract fun pexelsDetailActivity(): PexelsDetailActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = [(SearchModule::class)])
    internal abstract fun searchActivity(): SearchActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = [(AboutModule::class)])
    internal abstract fun aboutActivity(): AboutActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = [(SettingsModule::class)])
    internal abstract fun settingsActivity(): SettingsActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = [(SettingsModule::class)])
    internal abstract fun splashActivity(): SplashActivity

}