package learn.app.papewall.di.modules

import dagger.Module
import dagger.Provides
import learn.app.papewall.api.services.UnsplashService
import learn.app.papewall.api.RequestInterceptor
import learn.app.papewall.api.services.PexelsService
import learn.app.papewall.api.services.PixabayService
import learn.app.papewall.utils.Constants
import learn.app.papewall.utils.Constants.APPLICATION_ID_PEXELS
import learn.app.papewall.utils.Constants.APPLICATION_ID_PIXABAY
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

/**
 *
 *
 * this class has all the dependencies that we need through the lifecycle of our app
 * such as retrofit api service, http client, database dao, room database, etc.
 */

@Module
class NetworkModule() {
    companion object {
        private const val TAG = "NetworkModule"
        private const val REQUEST_TIMEOUT = 60
    }

    @Provides
    @Singleton
    @Named("unsplash_http")
    fun provideUnsplashOkHttpClient(requestInterceptor: RequestInterceptor): OkHttpClient {

        val logger = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger {
            Timber.d(it)
        })
        logger.level = HttpLoggingInterceptor.Level.BASIC

        return OkHttpClient.Builder()
            .addInterceptor(requestInterceptor)
            .addInterceptor(logger)
            .addInterceptor { chain ->
                val request =
                    chain.request().newBuilder().addHeader("Authorization", "Client-ID " + Constants.APPLICATION_ID_UNSPLASH)
                        .build()
                chain.proceed(request)
            }
            .connectTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .readTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .writeTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .build()
    }

    @Provides
    @Singleton
    @Named("pixabay_http")
    fun providePixabayOkHttpClient(requestInterceptor: RequestInterceptor): OkHttpClient {

        val logger = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger {
            Timber.d(it)
        })
        logger.level = HttpLoggingInterceptor.Level.BASIC

        return OkHttpClient.Builder()
            .addInterceptor(requestInterceptor)
            .addInterceptor(logger)
            .addInterceptor { chain ->
                val request =
                    chain.request().newBuilder().addHeader("key", APPLICATION_ID_PIXABAY)
                        .build()
                chain.proceed(request)
            }
            .connectTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .readTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .writeTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .build()
    }

    @Provides
    @Singleton
    @Named("pexels_http")
    fun providePexelsOkHttpClient(requestInterceptor: RequestInterceptor): OkHttpClient {

        val logger = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger {
            Timber.d(it)
        })
        logger.level = HttpLoggingInterceptor.Level.BASIC

        return OkHttpClient.Builder()
            .addInterceptor(requestInterceptor)
            .addInterceptor(logger)
            .addInterceptor { chain ->
                val request =
                    chain.request().newBuilder().addHeader("Authorization", APPLICATION_ID_PEXELS)
                        .build()
                chain.proceed(request)
            }
            .connectTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .readTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .writeTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .build()
    }

    //
    @Provides
    @Singleton
    fun provideUnsplashApiService(@Named("unsplash_http") okHttpClient: OkHttpClient): UnsplashService {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_UNSPLASH)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build()

        return retrofit.create(UnsplashService::class.java)
    }

    @Provides
    @Singleton
    fun providePixabayApiService(@Named("pixabay_http") okHttpClient: OkHttpClient): PixabayService {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_PIXABAY)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build()

        return retrofit.create(PixabayService::class.java)
    }

    @Provides
    @Singleton
    fun providePexelsApiService(@Named("pexels_http") okHttpClient: OkHttpClient): PexelsService {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_PEXELS)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build()

        return retrofit.create(PexelsService::class.java)
    }

//    @Provides
//    @Singleton
//    fun provideFirebaseAnalytics(context: Context): FirebaseAnalytics {
//        return FirebaseAnalytics.getInstance(context)
//    }
}