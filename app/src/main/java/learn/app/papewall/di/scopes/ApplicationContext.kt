package learn.app.papewall.di.scopes

@ApplicationContext
@Retention(AnnotationRetention.RUNTIME)
annotation class ApplicationContext