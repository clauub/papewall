package learn.app.papewall.di.components

import android.app.Application
import learn.app.papewall.di.modules.ActivityBindingModule
import learn.app.papewall.di.modules.NetworkModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import learn.app.papewall.App
import javax.inject.Singleton

/**
 *
 * This is the main injection class that injects [learn.app.tech.di.components] Activities, and android modules.
 */
@Singleton
@Component(modules = [ActivityBindingModule::class,
    AndroidSupportInjectionModule::class,
    NetworkModule::class])
interface AppComponent : AndroidInjector<App> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}