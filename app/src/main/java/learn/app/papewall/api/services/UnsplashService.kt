package learn.app.papewall.api.services

import io.reactivex.Observable
import learn.app.papewall.model.Example
import learn.app.papewall.model.Photo
import learn.app.papewall.utils.Constants
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface UnsplashService {

    @GET("photos")
    fun getPhotos(@Query("page") page: Int): Call<List<Photo>>

    @GET("photos/curated")
    fun getCuratedPhotos(@Query("page") page: Int): Call<List<Photo>>

    @GET("photos/random")
    fun getRandomPhotos(@Query("count") count: Int, @Query("query") query: String, @Query("orientation") orientation: String): Observable<List<Photo>>

    @GET("photos/featured")
    fun getFeaturedCollections(@Query("page") page: Int): Call<List<Photo>>

    @GET("search/photos")
    fun searchPhotos(@Query("page") page: Int, @Query("query") query: String, @Query("orientation") orientation: String): Call<Example>
}