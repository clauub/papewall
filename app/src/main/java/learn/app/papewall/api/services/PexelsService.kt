package learn.app.papewall.api.services

import learn.app.papewall.model.Example
import learn.app.papewall.model.PexelsResults
import learn.app.papewall.model.PixabayResults
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface PexelsService {

    @GET("search")
    fun searchPexelsPhotos(@Query("query") query: String, @Query("page") page: Int): Call<PexelsResults>


}