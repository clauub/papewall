package learn.app.papewall.api.services

import learn.app.papewall.model.Example
import learn.app.papewall.model.Photo
import learn.app.papewall.model.PixabayResults
import learn.app.papewall.utils.Constants.APPLICATION_ID_PIXABAY
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface PixabayService {

    @GET("api/?")
    fun searchPixabayPhotos(
        @Query("key") key: String,
        @Query("q", encoded = true) query: String,
        @Query("page") page: Int,
        @Query("order") order: String,
        @Query("orientation") orientation: String,
        @Query("image_type") image_type: String
    ): Call<PixabayResults>
}