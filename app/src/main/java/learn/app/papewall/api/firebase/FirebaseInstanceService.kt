package learn.app.papewall.api.firebase

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Build
import androidx.core.app.NotificationCompat
import com.google.firebase.iid.FirebaseInstanceIdReceiver
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import learn.app.papewall.R
import timber.log.Timber

class FirebaseInstanceService : FirebaseMessagingService() {

    lateinit var notificationChannel: NotificationChannel
    lateinit var builder: Notification.Builder

    override fun onMessageReceived(p0: RemoteMessage?) {
        super.onMessageReceived(p0)

        if (p0?.data != null) {
            sendNotification(p0)
        }
    }

    private fun sendNotification(p0: RemoteMessage) {
        val title = p0.notification?.title
        val content = p0.notification?.body

        val notificationManager = this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val channelID = "PapeWall"

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            notificationChannel =
                NotificationChannel(channelID, "PapeWall Notification", NotificationManager.IMPORTANCE_HIGH)
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.GREEN
            notificationChannel.enableVibration(false)
            notificationManager.createNotificationChannel(notificationChannel)
        }

        builder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Notification.Builder(this, channelID)
                .setContentTitle(title)
                .setContentText(content)
                .setColor(resources.getColor(R.color.colorAccent))
                .setSmallIcon(R.drawable.ic_notification)
                .setLargeIcon(BitmapFactory.decodeResource(this.resources, R.drawable.ic_notification))
        } else {

            Notification.Builder(this)
                .setContentTitle(title)
                .setContentText(content)
                .setColor(resources.getColor(R.color.colorAccent))
                .setSmallIcon(R.drawable.ic_notification)
                .setLargeIcon(BitmapFactory.decodeResource(this.resources, R.drawable.ic_notification))
        }


        notificationManager.notify(1234, builder.build())

    }

    @SuppressLint("TimberArgCount")
    override fun onNewToken(p0: String?) {
        super.onNewToken(p0)

        Timber.d("TokenFirebase", p0)
    }

}