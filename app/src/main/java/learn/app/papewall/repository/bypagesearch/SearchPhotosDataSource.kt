package learn.app.papewall.repository.bypagesearch

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import learn.app.papewall.model.*
import learn.app.papewall.repository.NetworkState
import learn.app.papewall.repository.bypage.PhotosRemoteDataSource
import learn.app.papewall.utils.Constants.APPLICATION_ID_PIXABAY
import learn.app.papewall.utils.Constants.PHOTO_TYPE
import learn.app.papewall.utils.Constants.PORTRAIT
import learn.app.papewall.utils.Constants.VERTICAL
import retrofit2.Call
import retrofit2.Response
import java.io.IOException

class SearchPhotosDataSource(
    private val dataSource: PhotosRemoteDataSource,
    private val query: String
) : PageKeyedDataSource<Int, PexelsPhoto>() {

    val networkState = MutableLiveData<NetworkState>()

    val initialLoad = MutableLiveData<NetworkState>()

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, PexelsPhoto>) {
        networkState.postValue(NetworkState.LOADING)
        initialLoad.postValue(NetworkState.LOADING)

        // triggered by a refresh, we better execute sync
        try {
            val response = fetchPhotos(1).execute()
            if (response.isSuccessful) {
                val data = response.body()?.photos
                val items = data?.map { it } ?: emptyList()
                networkState.postValue(NetworkState.LOADED)
                initialLoad.postValue(NetworkState.LOADED)
                callback.onResult(items, null, 2)
            } else {
                initNetworkError("error code: ${response.code()} " + response.message())
            }
        } catch (ioException: IOException) {
            initNetworkError(ioException.message ?: "unknown error")
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, PexelsPhoto>) {
        networkState.postValue(NetworkState.LOADING)
        fetchPhotos(params.key).enqueue(
            object : retrofit2.Callback<PexelsResults> {
                override fun onFailure(call: Call<PexelsResults>, t: Throwable) {
                    networkState.postValue(
                        NetworkState.error(
                            t.message ?: "unknown err"
                        )
                    )
                }

                override fun onResponse(
                    call: Call<PexelsResults>,
                    response: Response<PexelsResults>
                ) {
                    if (response.isSuccessful) {
                        val data = response.body()?.photos
                        val items = data?.map { it } ?: emptyList()
                        callback.onResult(items, params.key + 1)
                        networkState.postValue(NetworkState.LOADED)
                    } else {
                        networkState.postValue(
                            NetworkState.error(
                                "error code: ${response.code()} " + response.message()
                            )
                        )
                    }
                }
            }
        )
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, PexelsPhoto>) {

    }

    private fun fetchPhotos(page: Int): Call<PexelsResults> {
        if (query.isNotEmpty()) {
            return dataSource.fetchPexelsSearch(
                query = query, page = page
            )
        }
        throw RuntimeException("Unknown state to fetch photos")
    }

    private fun initNetworkError(msg: String) {
        val error = NetworkState.error(msg)
        networkState.postValue(error)
        initialLoad.postValue(error)
    }
}