package learn.app.papewall.repository.bypagesearchpixabay

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import learn.app.papewall.model.PexelsPhoto
import learn.app.papewall.model.PixabayPhoto
import learn.app.papewall.repository.bypage.PhotosRemoteDataSource
import learn.app.papewall.repository.bypagesearch.SearchPhotosDataSource

class SearchPhotoDataSourceFactoryPixabay(
private val dataSource: PhotosRemoteDataSource,
private val query: String) : DataSource.Factory<Int, PixabayPhoto>() {
    val sourceLiveData = MutableLiveData<SearchPhotosDataSourcePixabay>()
    override fun create(): DataSource<Int, PixabayPhoto> {
        val source = SearchPhotosDataSourcePixabay(
            dataSource = dataSource,
            query = query)
        sourceLiveData.postValue(source)
        return source
    }
}