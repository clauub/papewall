package learn.app.papewall.repository.bypage

import androidx.annotation.MainThread
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import learn.app.papewall.model.Photo
import learn.app.papewall.repository.Listing
import learn.app.papewall.repository.UnsplashRepo
import learn.app.papewall.utils.SortType
import java.util.concurrent.Executor

class PageKeyRepository(private val dataSource: PhotosRemoteDataSource,
                        private val sortType: SortType?,
                        private val networkExecutor: Executor): UnsplashRepo {

    @MainThread
    override fun getPhotos(query: String, pageSize: Int): Listing<Photo> {

        val sourceFactory = PhotoDataSourceFactory(
            dataSource = dataSource,
            sortType = sortType,
            query = query
        )

        val livePagedList = LivePagedListBuilder(sourceFactory, pageSize)
            // provide custom executor for network requests, otherwise it will default to
            // Arch Components' IO pool which is also used for disk access
            .setFetchExecutor(networkExecutor)
            .build()

        val refreshState = Transformations.switchMap(sourceFactory.sourceLiveData) {
            it.initialLoad
        }
        return Listing(
            pagedList = livePagedList,
            networkState = Transformations.switchMap(sourceFactory.sourceLiveData) {
                it.networkState
            },
            refresh = {
                sourceFactory.sourceLiveData.value?.invalidate()
            },
            refreshState = refreshState
        )
    }

}
