package learn.app.papewall.repository.bypage

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import learn.app.papewall.model.Photo
import learn.app.papewall.utils.SortType

class PhotoDataSourceFactory(
    private val dataSource: PhotosRemoteDataSource,
    private val sortType: SortType?,
    private val query : String) : DataSource.Factory<Int, Photo>() {
    val sourceLiveData = MutableLiveData<PhotosDataSource>()
    override fun create(): DataSource<Int, Photo> {
        val source = PhotosDataSource(
            dataSource = dataSource,
            sortType = sortType,
            query = query
        )
        sourceLiveData.postValue(source)
        return source
    }
}