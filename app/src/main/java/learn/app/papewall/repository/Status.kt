package learn.app.papewall.repository

enum class Status {
     RUNNING,
     SUCCESS,
     FAILED
}