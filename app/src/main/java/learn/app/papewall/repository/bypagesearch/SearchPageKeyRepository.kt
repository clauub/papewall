package learn.app.papewall.repository.bypagesearch

import androidx.annotation.MainThread
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import learn.app.papewall.model.PexelsPhoto
import learn.app.papewall.model.PixabayPhoto
import learn.app.papewall.repository.Listing
import learn.app.papewall.repository.PexelsRepo
import learn.app.papewall.repository.PixabayRepo
import learn.app.papewall.repository.UnsplashRepo
import learn.app.papewall.repository.bypage.PhotosRemoteDataSource
import java.util.concurrent.Executor


class SearchPageKeyRepository(private val dataSource: PhotosRemoteDataSource,
                              private val networkExecutor: Executor
) : PexelsRepo {

    @MainThread
    override fun getPexelsPhotos(query: String, pageSize: Int): Listing<PexelsPhoto> {

        val sourceFactory = SearchPhotoDataSourceFactory(
                dataSource = dataSource,
                query = query)

        val livePagedList = LivePagedListBuilder(sourceFactory, pageSize)
                // provide custom executor for network requests, otherwise it will default to
                // Arch Components' IO pool which is also used for disk access
                .setFetchExecutor(networkExecutor)
                .build()

        val refreshState = Transformations.switchMap(sourceFactory.sourceLiveData) {
            it.initialLoad
        }
        return Listing(
                pagedList = livePagedList,
                networkState = Transformations.switchMap(sourceFactory.sourceLiveData) {
                    it.networkState
                },
                refresh = {
                    sourceFactory.sourceLiveData.value?.invalidate()
                },
                refreshState = refreshState
        )
    }

}
