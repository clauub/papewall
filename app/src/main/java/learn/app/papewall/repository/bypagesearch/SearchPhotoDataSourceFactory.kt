package learn.app.papewall.repository.bypagesearch

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import learn.app.papewall.model.PexelsPhoto
import learn.app.papewall.model.Photo
import learn.app.papewall.model.PixabayPhoto
import learn.app.papewall.model.PixabayResults
import learn.app.papewall.repository.bypage.PhotosRemoteDataSource

class SearchPhotoDataSourceFactory(
        private val dataSource: PhotosRemoteDataSource,
        private val query: String) : DataSource.Factory<Int, PexelsPhoto>() {
    val sourceLiveData = MutableLiveData<SearchPhotosDataSource>()
    override fun create(): DataSource<Int, PexelsPhoto> {
        val source = SearchPhotosDataSource(
                dataSource = dataSource,
                query = query)
        sourceLiveData.postValue(source)
        return source
    }
}