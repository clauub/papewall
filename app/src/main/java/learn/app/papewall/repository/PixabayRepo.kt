package learn.app.papewall.repository

import learn.app.papewall.model.PixabayPhoto

interface PixabayRepo {

    fun getPixabayPhotos(query: String, pageSize: Int): Listing<PixabayPhoto>
}