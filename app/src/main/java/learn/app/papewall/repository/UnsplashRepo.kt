package learn.app.papewall.repository

import learn.app.papewall.model.Photo
import learn.app.papewall.model.PixabayPhoto

interface UnsplashRepo {
    fun getPhotos(query: String, pageSize: Int): Listing<Photo>
}
