package learn.app.papewall.repository.bypage

import io.reactivex.Observable
import learn.app.papewall.api.services.PexelsService
import learn.app.papewall.api.services.PixabayService
import learn.app.papewall.api.services.UnsplashService
import learn.app.papewall.model.Example
import learn.app.papewall.model.PexelsResults
import learn.app.papewall.model.Photo
import learn.app.papewall.model.PixabayResults
import learn.app.papewall.utils.SortType
import retrofit2.Call
import javax.inject.Inject

class PhotosRemoteDataSource @Inject constructor(
    private val photoService: UnsplashService,
    private val photoPixabayService: PixabayService,
    private val pexelsService: PexelsService
) {

    fun fetchPhotos(sortType: SortType, page: Int): Call<List<Photo>> {
        return when (sortType) {
            SortType.MOST_POPULAR -> photoService.getPhotos(page)
            SortType.HIGHEST_RATED -> photoService.getCuratedPhotos(page)
        }
    }

    fun fetchSearch(page: Int, query: String, orientation: String): Call<Example> {
        return photoService.searchPhotos(page, query, orientation)
    }

    fun fetchPixabaySearch(
        key: String,
        page: Int,
        query: String,
        order: String,
        orientation: String,
        image_type: String
    ): Call<PixabayResults> {
        return photoPixabayService.searchPixabayPhotos(key, query, page,order,  orientation, image_type)
    }

    fun fetchPexelsSearch(query: String, page: Int): Call<PexelsResults> {
        return pexelsService.searchPexelsPhotos(query, page)
    }

    fun randomPhotos(count: Int, query: String, orientation: String): Observable<List<Photo>> {
        return photoService.getRandomPhotos(count, query, orientation)
    }
}