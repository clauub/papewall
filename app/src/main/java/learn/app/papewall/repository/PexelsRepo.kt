package learn.app.papewall.repository

import learn.app.papewall.model.PexelsPhoto
import learn.app.papewall.model.PixabayPhoto

interface PexelsRepo {

    fun getPexelsPhotos(query: String, pageSize: Int): Listing<PexelsPhoto>
}