package learn.app.papewall.model

import android.os.Parcelable
import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

@Parcelize
data class Photo(
    @SerializedName("id")
    @Expose
    var id: String? = null,
    @SerializedName("created_at")
    @Expose
    var createdAt: String? = null,
    @SerializedName("width")
    @Expose
    var width: Int? = null,
    @SerializedName("height")
    @Expose
    var height: Int? = null,
    @SerializedName("color")
    @Expose
    var color: String? = null,
    @SerializedName("likes")
    @Expose
    var likes: Int? = null,
    @SerializedName("liked_by_user")
    @Expose
    var likedByUser: Boolean? = null,
    @SerializedName("description")
    @Expose
    var description: String? = null,
    @SerializedName("user")
    @Expose
    var user: @RawValue User? = null,
    @SerializedName("current_user_collections")
    @Expose
    var currentUserCollections: @RawValue List<Any>? = null,
    @SerializedName("urls")
    @Expose
    var urls:  @RawValue Urls? = null,
    @SerializedName("links")
    @Expose
    var links:  @RawValue Links_? = null): Parcelable


@Parcelize
data class Urls(
    @SerializedName("raw")
    @Expose
    var raw: String? = null,
    @SerializedName("full")
    @Expose
    var full: String? = null,
    @SerializedName("regular")
    @Expose
    var regular: String? = null,
    @SerializedName("small")
    @Expose
    var small: String? = null,
    @SerializedName("thumb")
    @Expose
    var thumb: String? = null) : Parcelable

@Parcelize
data class User(

    @SerializedName("id")
    @Expose
    var id: String? = null,
    @SerializedName("username")
    @Expose
    var username: String? = null,
    @SerializedName("name")
    @Expose
    var name: String? = null,
    @SerializedName("first_name")
    @Expose
    var firstName: String? = null,
    @SerializedName("last_name")
    @Expose
    var lastName: String? = null,
    @SerializedName("instagram_username")
    @Expose
    var instagramUsername: String? = null,
    @SerializedName("twitter_username")
    @Expose
    var twitterUsername: String? = null,
    @SerializedName("portfolio_url")
    @Expose
    var portfolioUrl: String? = null,
    @SerializedName("profile_image")
    @Expose
    var profileImage: @RawValue  ProfileImage? = null,
    @SerializedName("links")
    @Expose
    var links: @RawValue Links? = null) : Parcelable

@Parcelize
data class ProfileImage(

    @SerializedName("small")
    @Expose
    var small: String? = null,
    @SerializedName("medium")
    @Expose
    var medium: String? = null,
    @SerializedName("large")
    @Expose
    var large: String? = null) : Parcelable

@Parcelize
data class Links_ (
    @SerializedName("self")
    @Expose
    var self: String? = null,
    @SerializedName("html")
    @Expose
    var html: String? = null,
    @SerializedName("download")
    @Expose
    var download: String? = null): Parcelable

@Parcelize
data class Links (

    @SerializedName("self")
    @Expose
    var self: String? = null,
    @SerializedName("html")
    @Expose
    var html: String? = null,
    @SerializedName("photos")
    @Expose
    var photos: String? = null,
    @SerializedName("likes")
    @Expose
    var likes: String? = null): Parcelable

@Parcelize
data class Example (

    @SerializedName("total")
    @Expose
    var total: Int? = null,
    @SerializedName("total_pages")
    @Expose
    var totalPages: Int? = null,
    @SerializedName("results")
    @Expose
    var results: List<Photo>? = null) : Parcelable