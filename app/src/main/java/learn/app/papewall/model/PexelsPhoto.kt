package learn.app.papewall.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

@Parcelize
data class PexelsPhoto(
    @SerializedName("id")
    @Expose
    val id: String? = null,
    @SerializedName("width")
    @Expose
    val width: Int? = null,
    @SerializedName("height")
    @Expose
    val height: Int? = null,
    @SerializedName("url")
    @Expose
    val url: String? = null,
    @SerializedName("photographer")
    @Expose
    val photographer: String? = null,
    @SerializedName("photographer_url")
    @Expose
    val photographer_url: String? = null,
    @SerializedName("src")
    @Expose
    val src: @RawValue Src? = null
) : Parcelable

@Parcelize
data class Src(
    @SerializedName("original")
    @Expose
    val original: String? = null,
    @SerializedName("large2x")
    @Expose
    val large2x: String? = null,
    @SerializedName("large")
    @Expose
    val large: String? = null,
    @SerializedName("medium")
    @Expose
    val medium: String? = null,
    @SerializedName("small")
    @Expose
    val small: String? = null,
    @SerializedName("portrait")
    @Expose
    val portrait: String? = null,
    @SerializedName("square")
    @Expose
    val square: String? = null,
    @SerializedName("landscape")
    @Expose
    val landscape: String? = null,
    @SerializedName("tiny")
    @Expose
    val tiny: String? = null
) : Parcelable

@Parcelize
data class PexelsResults(

    @SerializedName("total_results")
    @Expose
    var totalResults: Int? = null,
    @SerializedName("page")
    @Expose
    var page: Int? = null,
    @SerializedName("per_page")
    @Expose
    var per_page: Int? = null,
    @SerializedName("photos")
    @Expose
    var photos: List<PexelsPhoto>? = null
) : Parcelable
