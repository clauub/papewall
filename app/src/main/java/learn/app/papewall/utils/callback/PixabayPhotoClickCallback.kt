package learn.app.papewall.utils.callback

import android.widget.ImageView
import learn.app.papewall.model.Photo
import learn.app.papewall.model.PixabayPhoto

interface PixabayPhotoClickCallback {
    fun onClick(photo: PixabayPhoto, poster: ImageView)
}