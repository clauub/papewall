package learn.app.papewall.utils

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager


class SharedPref(context: Context) {
    private var mySharedPref: SharedPreferences = context.getSharedPreferences("filename", Context.MODE_PRIVATE)

    private lateinit var setPreferences: SharedPreferences

    // this method will save the nightMode State : True or False
    fun setNightModeState(state: Boolean?) {
        val editor = mySharedPref.edit()
        editor.putBoolean("Theme", state!!)
        editor.apply()
    }

    // this method will load the Night Mode State
    fun loadNightModeState(): Boolean? {
        return mySharedPref.getBoolean("Theme", false)
    }

}