package learn.app.papewall.utils.callback

import android.widget.ImageView
import learn.app.papewall.model.PexelsPhoto
import learn.app.papewall.model.PixabayPhoto

interface PexelsPhotoClickCallback {
    fun onClick(photo: PexelsPhoto, poster: ImageView)
}