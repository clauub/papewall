package learn.app.papewall.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import learn.app.papewall.R
import learn.app.papewall.ui.settings.SettingsActivity


class FragmentHelper {

    fun getFragmentManager(context: Context): FragmentManager {
        return (context as AppCompatActivity).supportFragmentManager
    }

    fun openFragment(context: Context, frameId: Int, fragment: Fragment) {
        getFragmentManager(context).beginTransaction()
            .replace(frameId, fragment, fragment.toString())
            .addToBackStack(null).commit()
    }

    fun addFragment(context: Context, frameId: Int, fragment: Fragment) {
        getFragmentManager(context).beginTransaction()
            .add(frameId, fragment, fragment.toString())
            .addToBackStack(null).commit()
    }

    fun changeFragmentActivity(context: Context, fragment: Fragment, frameId: Int) {
        getFragmentManager(context).beginTransaction()
            .replace(frameId, fragment)
            .addToBackStack(null)
            .commit()
    }
}