package learn.app.papewall.utils

import android.content.Context
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd

class AdsUtils {

    private var adRequest: AdRequest? = null
    private var mInterstitialAd: InterstitialAd? = null

    fun adRequestBanner(): AdRequest? {
//        adRequest = AdRequest.Builder().build()
        adRequest = AdRequest.Builder().addTestDevice("135F1B9563701B763CCE2B7708DD997C").build()
        return adRequest
    }

    fun adRequestInterstitial(context: Context, string: String): InterstitialAd? {
        mInterstitialAd = InterstitialAd(context)
        mInterstitialAd?.adUnitId = string
        mInterstitialAd?.loadAd(AdRequest.Builder().addTestDevice("135F1B9563701B763CCE2B7708DD997C").build())
        return mInterstitialAd
    }
}