package learn.app.papewall.utils

enum class SortType(val value: Int) {

    MOST_POPULAR(0),
    HIGHEST_RATED(1),
}