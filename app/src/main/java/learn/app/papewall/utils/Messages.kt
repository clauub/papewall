package learn.app.papewall.utils

import android.content.Context
import android.content.Intent
import android.view.View
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_main_photo.*
import learn.app.papewall.R
import timber.log.Timber
import android.widget.FrameLayout

object Messages {

    fun snackBarLong(view: View, message: Int) {
        Snackbar.make(view, message, Snackbar.LENGTH_LONG).show()
    }

    fun snackBarShort(view: View, message: Int) {
        Snackbar.make(view, message, Snackbar.LENGTH_SHORT).show()
    }

    fun toastLong(context: Context, message: Int) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    fun toastShort(context: Context, message: Int) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    fun timberE(message: String?) {
        Timber.e(message)
    }

    private fun displaySnackBarWithBottomMargin(snackbar: Snackbar, sideMargin: Int, marginBottom: Int) {
        val snackBarView = snackbar.view
        val params = snackBarView.layoutParams as FrameLayout.LayoutParams

        params.setMargins(
            params.leftMargin + sideMargin,
            params.topMargin,
            params.rightMargin + sideMargin,
            params.bottomMargin + marginBottom
        )

        snackBarView.layoutParams = params
        snackbar.show()
    }
}

