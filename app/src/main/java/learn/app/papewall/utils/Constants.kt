package learn.app.papewall.utils

object Constants {

    const val APPLICATION_ID_UNSPLASH = "50c1134e3639717a854960ff1b64ce77183049643d4efc714eb7407a6bccdf2a"
    const val APPLICATION_ID_PIXABAY = "11678771-17177c7a6a900f01c82db013f"
    const val APPLICATION_ID_PEXELS = "563492ad6f91700001000001ffbfc2e7bcc14766907289fa9c94269b"
    const val WALLPAPERS_STRING = "wallpapers"

    //    PEXELS STRINGS
    const val SHARE_WALLPAPER_PEXELS = "share_wallpaper_pexels"
    const val DOWNLOAD_WALLPAPER_PEXELS = "download_wallpaper_pexels"
    const val SET_WALLPAPER_PEXELS = "set_wallpaper_pexels"
    const val BASE_URL_PEXELS = "https://api.pexels.com/v1/"

    //    UNSPLASH STRINGS
    const val SHARE_WALLPAPER_UNSPLASH = "share_wallpaper_unsplash"
    const val DOWNLOAD_WALLPAPER_UNSPLASH = "download_wallpaper_unsplash"
    const val SET_WALLPAPER_UNSPLASH = "set_wallpaper_unsplash"
    const val BASE_URL_UNSPLASH = "https://api.unsplash.com/"

    //    PIXABAY STRINGS
    const val SHARE_WALLPAPER_PIXABAY = "share_wallpaper_pixabay"
    const val DOWNLOAD_WALLPAPER_PIXABAY = "download_wallpaper_pixabay"
    const val SET_WALLPAPER_PIXABAY = "set_wallpaper_pixabay"
    const val BASE_URL_PIXABAY = "https://pixabay.com/"


    //   CATEGORIES STRINGS
    const val ANDROID_WALLPAPERS = "Android Wallpapers"
    const val IPHONE_WALLPAPERS = "iPhone Wallpapers"
    const val FOOD_WALLPAPERS = "Food"
    const val ARHITECTURE_WALLPAPERS = "Architecture"
    const val ANIMALS_WALLPAPERS = "Animals"
    const val TRAVEL_WALLPAPERS = "travel"
    const val NATURE_WALLPAPERS = "Nature"
    const val ABSTRACT_WALLPAPERS = "Abstract"
    const val MINIMAL_WALLPAPERS = "Minimal"

    //    IMAGES STRINGS
    const val PHOTO_TYPE = "photo"
    const val LATEST = "latest"
    const val PORTRAIT = "portrait"
    const val VERTICAL = "vertical"

    //    ADS STRINGS
    const val BACK_BUTTON_DETAIL_INTERSTITIAL = "ca-app-pub-2854051253644042/3834025072"

}