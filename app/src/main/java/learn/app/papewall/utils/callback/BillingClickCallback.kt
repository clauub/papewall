package learn.app.papewall.utils.callback

import com.android.billingclient.api.SkuDetails
import learn.app.papewall.repository.billing.localdb.AugmentedSkuDetails

interface BillingClickCallback {
    fun onClick(skuDetails: AugmentedSkuDetails)
}