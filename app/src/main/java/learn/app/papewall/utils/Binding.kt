package learn.app.papewall.utils

import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.constraintlayout.widget.Placeholder
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.mikhaellopez.circularimageview.CircularImageView
import learn.app.papewall.widget.AutofitRecyclerView


object Binding {

    @BindingAdapter("app:visibleGone")
    @JvmStatic
    fun visibleGone(view: LinearLayout, isLoading: Boolean) {
        view.visibility = if (isLoading) View.VISIBLE else View.GONE
    }

    @BindingAdapter("app:visibleGone")
    @JvmStatic
    fun visibleGone(view: AutofitRecyclerView, isLoading: Boolean) {
        view.visibility = if (isLoading) View.VISIBLE else View.GONE
    }

    @JvmStatic
    @BindingAdapter("app:imageUrl", "placeholder")
    fun bindImage(imageView: ImageView, url: String?, placeholder: Drawable) {
        Glide.with(imageView.context)
            .setDefaultRequestOptions(RequestOptions().placeholder(placeholder).diskCacheStrategy(DiskCacheStrategy.ALL))
            .load(url)
            .into(imageView)
    }

    @JvmStatic
    @BindingAdapter("app:circularImageUrl")
    fun bindCircularImage(circularImageView: CircularImageView, url: String?) {
        Glide.with(circularImageView.context)
            .load(url)
            .into(circularImageView)
    }
}