package learn.app.papewall.utils

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.widget.Toast
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.content.Context.CONNECTIVITY_SERVICE
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Point
import android.net.Uri
import android.os.Build
import android.util.Log
import android.view.WindowManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.*
import androidx.core.content.PermissionChecker
import learn.app.papewall.R
import learn.app.papewall.utils.Messages.toastLong
import timber.log.Timber
import java.io.File
import java.nio.file.Files.delete
import java.nio.file.Files.isDirectory
import androidx.core.content.ContextCompat.startActivity
import com.google.android.gms.ads.AdRequest
import learn.app.papewall.utils.Messages.timberE
import android.view.View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
import android.view.View.SYSTEM_UI_FLAG_FULLSCREEN
import android.view.View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
import android.view.View


@Suppress("DEPRECATED_IDENTITY_EQUALS")
class Utils {

    private var sharedpref: SharedPref? = null
    private var adRequest: AdRequest? = null

    // return true if internet cnx is available on device
    fun isConnected(context: Context?): Boolean {
        if (context == null)
            return false
        val connectivityManager = context.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        if (networkInfo != null && (networkInfo.type == ConnectivityManager.TYPE_WIFI || networkInfo.type == ConnectivityManager.TYPE_MOBILE) && networkInfo.isConnected)
            return true
        toastLong(context, R.string.no_internet_connection)
        return false
    }

    // return true if internet cnx is available on device
    fun isConnectedNoToast(context: Context?): Boolean {
        if (context == null)
            return false
        val connectivityManager = context.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        if (networkInfo != null && (networkInfo.type == ConnectivityManager.TYPE_WIFI || networkInfo.type == ConnectivityManager.TYPE_MOBILE) && networkInfo.isConnected)
            return true
        timberE(R.string.no_internet_connection.toString())
        return false
    }

    @SuppressLint("ObsoleteSdkInt")
    fun checkPermission(activity: Activity): Boolean {
        if (Build.VERSION.SDK_INT >= 23) return if (PermissionChecker.checkSelfPermission(
                activity,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
            === PackageManager.PERMISSION_GRANTED &&
            PermissionChecker.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            === PackageManager.PERMISSION_GRANTED
        ) {

            true
        } else {
            ActivityCompat.requestPermissions(
                activity,
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE),
                3
            )
            false
        } else { //permission is automatically granted on sdk<23 upon installation
            return true
        }
    }

    fun checkAndRequest(context: Activity) {
        if (checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                context,
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                0
            )
        }
    }

    fun deleteCache(context: Context) {
        try {
            val dir = context.cacheDir
            deleteDir(dir)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun deleteDir(dir: File?): Boolean {
        if (dir != null && dir.isDirectory) {
            val children = dir.list()
            for (i in children.indices) {
                val success = deleteDir(File(dir, children[i]))
                if (!success) {
                    return false
                }
            }
            return dir.delete()
        } else return if (dir != null && dir.isFile) {
            dir.delete()
        } else {
            false
        }
    }

    fun changeTheme(context: Context) {
        sharedpref = SharedPref(context)
        if (sharedpref?.loadNightModeState() == true) {
            context.setTheme(R.style.anotherstyle)
        } else context.setTheme(R.style.AppTheme)
    }

    fun restartActivityFromFragment(activity: Activity) {
        val i = activity.baseContext.packageManager.getLaunchIntentForPackage(activity.baseContext.packageName)
        i!!.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        activity.startActivity(i)
        activity.finish()
    }

    fun openUrl(context: Context, string: String) {
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse(string)
        context.startActivity(i)
    }

    fun adRequest(): AdRequest? {
//        adRequest = AdRequest.Builder().build()
        adRequest = AdRequest.Builder().addTestDevice("135F1B9563701B763CCE2B7708DD997C").build()
        return adRequest
    }

}