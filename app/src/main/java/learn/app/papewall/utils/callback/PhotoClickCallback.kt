package learn.app.papewall.utils.callback

import android.widget.ImageView
import android.widget.TextView
import learn.app.papewall.model.Photo

interface PhotoClickCallback {
    fun onClick(photo: Photo, poster: ImageView)
}
