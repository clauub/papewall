package learn.app.papewall

import android.app.Activity
import android.app.Application
import androidx.multidex.BuildConfig
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.facebook.stetho.Stetho
import com.google.android.gms.ads.MobileAds
import com.squareup.leakcanary.LeakCanary
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import learn.app.papewall.di.components.AppComponent
import learn.app.papewall.di.components.DaggerAppComponent
import javax.inject.Inject
import timber.log.Timber
import com.google.firebase.analytics.FirebaseAnalytics
import learn.app.papewall.repository.billing.localdb.LocalBillingDb


class App : DaggerApplication() {

    private var mFirebaseAnalytics: FirebaseAnalytics? = null

    override fun onCreate() {
        super.onCreate()

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)
        MultiDex.install(this)

        MobileAds.initialize(this, "ca-app-pub-2854051253644042~5945734438")

        if (learn.app.papewall.BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
            Stetho.initializeWithDefaults(this)
            if (LeakCanary.isInAnalyzerProcess(this)) return
            LeakCanary.install(this)
        }
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder().application(this).build()
    }

    fun getmFirebaseAnalytics(): FirebaseAnalytics {
        return mFirebaseAnalytics!!
    }


}